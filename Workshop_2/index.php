<?php
    echo "First part of the workshop::<br><br>";
    $ceu = array( "Italy"=>"Rome", "Luxembourg"=>"Luxembourg", "Belgium"=> "Brussels", "Denmark"=>"Copenhagen", "Finland"=>"Helsinki", "France" => "Paris", "Slovakia"=>"Bratislava", "Slovenia"=>"Ljubljana", "Germany" => "Berlin", "Greece" => "Athens", "Ireland"=>"Dublin", "Netherlands"=>"Amsterdam", "Portugal"=>"Lisbon", "Spain"=>"Madrid", "Sweden"=>"Stockholm", "United Kingdom"=>"London", "Cyprus"=>"Nicosia", "Lithuania"=>"Vilnius", "Czech Republic"=>"Prague", "Estonia"=>"Tallin", "Hungary"=>"Budapest", "Latvia"=>"Riga", "Malta"=>"Valetta", "Austria" => "Vienna", "Poland"=>"Warsaw");
    ksort($ceu);
    foreach ($ceu as $key => $value) {
        echo "The capital of $key is $value <br>";
    }
    echo "<br><hr><br>";
    echo "Second part of the workshop::<br><br>";
    $temperature = array(78, 60, 62, 68, 71, 68, 73, 85, 66, 64, 76, 63, 75, 76, 73, 68, 62, 73, 72, 65, 74, 62, 62, 65, 64, 68, 73, 75, 79, 73);
    $average = number_format((float)(array_sum($temperature) / count($temperature)), 2, ',', '');
    echo "Average Temperature is: ".$average;
    //
    $noduplicates = array_unique($temperature);
    sort($noduplicates);
    echo "<br>List of 5 lowest temperatures (no duplicates): ".implode(", ",array_slice($noduplicates, 0,5));
    echo "<br>List of 5 lowest temperatures (no duplicates): ".implode(", ",array_slice($noduplicates, -5));
?>