<?php
    session_start();
    require_once('../../include/dbconnection.php');
    $user = $_SESSION['user'];
    if (!$user) {
        header('Location: ../modules/auth/login_form.php');
    }
    if($user["role"] != "Administrador"){
        header('Location: /index.php');
    }
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="/assets/css/bootadmin.min.css">
    <title>E-shop</title>
</head>
<body class="bg-light">

<?php include '../../modules/template/navbar.php'; ?>

<div class="d-flex">
    <?php include '../../modules/template/sidebar.php'; ?>
    <div class="content p-4">
        <?php
            //Obtener datos de producto
            $dbConnection = new DbConnection();
            if(isset($_REQUEST["id"])){//verificar si viene el id de producto
                $sql = "SELECT productos.id,sku,productos.nombre as nombre_producto,descripcion,imagen,id_categoria,stock,precio,categorias.nombre as nombre_cat FROM productos INNER JOIN categorias ON productos.id_categoria = categorias.id where productos.id =".$_REQUEST['id'];
                $result = $dbConnection->getDbConnectionObject()->query($sql);
                if(mysqli_num_rows($result)==0){
                    //Si el id del request no existe en la BD regresar a la lista de productos
                    $dbConnection->closeConnection();
                    header('Location: products.php');
                }
                $producto = $result->fetch_array();
            }else{
                $producto = array("id"=>0,"sku"=>"","nombre_producto"=>"","descripcion"=>"","imagen"=>"","id_categoria"=>0,"stock"=>0,"precio"=>0,"nombre_cat"=>"");
            }
            
        ?>
        <h2 class="mb-4">Producto</h2>
        <div class="card mb-4">
            <div class="card-header bg-white font-weight-bold">
                <?php
                    if(isset($_REQUEST["id"])){
                        echo 'Editar Producto: '.$producto["nombre_producto"];
                    }else{
                        echo 'Crear Producto';
                    }
                ?>
            </div>
            <div class="card-body">
                <form method="post" action="" enctype="multipart/form-data" id="formproduct">
                    <div class="form-group">
                        <label for="sku">SKU</label>
                        <input type="text" class="form-control" id="sku" name="sku" value="<?php echo $producto['sku']; ?>" required="" placeholder="SKU">
                    </div>
                    <div class="form-group">
                        <label for="inputNombre">Nombre</label>
                        <input type="text" class="form-control" id="inputNombre" name="nombre" value="<?php echo $producto['nombre_producto']; ?>" required="" placeholder="Nombre">
                    </div>
                    <div class="form-group">
                        <label for="descripcion">Descripcion</label>
                        <input type="text" class="form-control" id="descripcion" name="descripcion" value="<?php echo $producto['descripcion']; ?>" placeholder="Descripcion">
                    </div>
                    <div class="form-group">
                        <label for="productImg">Imagen</label>
                        <input type="file" class="form-control" name="productImg" id="productImg">
                        <?php 
                            if(strlen($producto["imagen"])>3){
                                echo "<img class='form-control' style='max-width:130px;' src='/modules/products/".$producto["imagen"]."'>";
                            }
                        ?>
                    </div>
                    <div class="form-group">
                        <label for="categoriaPadre">Categoria</label>
                        <select class="form-control" name="categoria" id="categoria">
                            <option value="0">Seleccionar</option>
                            <?php 
                                $queryCategorias = "SELECT id,nombre FROM categorias";
                                $resultcat = $dbConnection->getDbConnectionObject()->query($queryCategorias);
                                while($row = $resultcat->fetch_array()){
                                    $select = "";
                                    if($row["id"] == $producto["id_categoria"]){
                                        $select = "selected";
                                    }
                                    echo "<option $select value='".$row["id"]."'>".$row["nombre"]."</option>";
                                }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="stock">Stock</label>
                        <input type="number" min="0" required="" class="form-control" id="stock" name="stock" value="<?php echo $producto['stock']; ?>" placeholder="Stock">
                    </div>
                    <div class="form-group">
                        <label for="precio">Precio $</label>
                        <input type="number" min="1" required="" step="any" class="form-control" id="precio" name="precio" value="<?php echo $producto['precio']; ?>" placeholder="Precio">
                    </div>
                    <button type="submit" class="btn btn-success">Guardar</button>
                </form>
            </div>
        </div>
    </div>
</div>
<?php 
    $dbConnection->closeConnection();
?>
<script src="/assets/js/jquery.min.js"></script>
<script src="/assets/js/bootstrap.bundle.min.js"></script>
<script src="/assets/js/bootadmin.min.js"></script>
<script src="/assets/js/sweetalert2.js"></script>
<script>
    $('#formproduct').submit( function( e ) {
        e.preventDefault();
        var id = '<?php echo $producto["id"]; ?>';
        var opcion = "editarProducto";
        if(id == "0"){
            opcion = "crearProducto";
        }
        var categoria = $("#categoria").val();
        if(categoria == 0){
            Swal.fire({
                icon: 'error',
                text: "Debe seleccionar una categoria"
            });
            return;
        }
        var data = new FormData(this);
        data.append("opcion", opcion);
        data.append("id", id);
        $.ajax( {
            url: 'ajax.php',
            type: 'POST',
            data: data,
            processData: false,
            contentType: false,
            success: function(result){
                if(result=='Ok'){
                    Swal.fire({
                        icon: 'success',
                        text: 'Producto guardado',
                        onClose: () => {
                            window.location = "products.php";
                        }
                    });
                }else{
                    Swal.fire({
                        icon: 'error',
                        text: result
                    });
                }
            }
        });
    } );
</script>
</body>
</html>