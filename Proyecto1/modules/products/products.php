<?php
    session_start();
    require_once('../../include/dbconnection.php');
    $user = $_SESSION['user'];
    if (!$user) {
        header('Location: ../modules/auth/login_form.php');
    }
    if($user["role"] != "Administrador"){
        header('Location: /index.php');
    }
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="/assets/css/bootadmin.min.css">
    <title>E-shop</title>
</head>
<body class="bg-light">

<?php include '../../modules/template/navbar.php'; ?>

<div class="d-flex">
    <?php include '../../modules/template/sidebar.php'; ?>
    <div class="content p-4">
        <?php
            //Obtener datos de dashboard
            $dbConnection = new DbConnection();
            $sql = "SELECT productos.id,sku,productos.nombre as nombre_producto,descripcion,imagen,id_categoria,stock,precio,categorias.nombre as nombre_cat FROM productos INNER JOIN categorias ON productos.id_categoria = categorias.id";
            $result = $dbConnection->getDbConnectionObject()->query($sql);
            $dbConnection->closeConnection();
        ?>
            <h2 class="mb-4">Productos</h2>

            <div class="card mb-4">
                <div class="card-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">SKU</th>
                                        <th scope="col">Nombre</th>
                                        <th scope="col">Descripción</th>
                                        <th scope="col">Categoria</th>
                                        <th scope="col">Stock</th>
                                        <th scope="col">Precio</th>
                                        <th scope="col">Editar</th>
                                        <th scope="col">Eliminar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        while($producto = $result->fetch_array()){
                                            echo "<tr>";
                                                echo "<td>".$producto['sku']."</td>";
                                                echo "<td><span id='product".$producto['id']."'>".$producto['nombre_producto']."</span></td>";
                                                echo "<td>".$producto['descripcion']."</td>";
                                                echo "<td>".$producto['nombre_cat']."</td>";
                                                echo "<td>".$producto['stock']."</td>";
                                                echo "<td>$".$producto['precio']."</td>";
                                                echo "<td><a href='products_form.php?id=".$producto['id']."'><button class='btn btn-primary'>Editar</button></a></td>";
                                                echo "<td><button class='btn btn-danger' onclick='EliminarProducto(".$producto['id'].")'>Eliminar</button></td>";
                                            echo "</tr>";
                                        }
                                    ?>
                                </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <a href="products_form.php"><button class="btn btn-success"><i class="fas fa-plus"></i> Nuevo Producto</button></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>

<script src="/assets/js/jquery.min.js"></script>
<script src="/assets/js/bootstrap.bundle.min.js"></script>
<script src="/assets/js/bootadmin.min.js"></script>
<script src="/assets/js/sweetalert2.js"></script>
<script>
    function EliminarProducto(id){
        //Preguntar para borrar la categoria
        Swal.fire({
            title: 'Eliminar producto '+$("#product"+id).html()+', esta seguro?',
            text: "Esto no se puede revertir!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Si, eliminar!'
            }).then((result) => {
                if (result.isConfirmed) {
                    jQuery.ajax({
                        url: "ajax.php",
                        method: 'POST',
                        async:true,
                        data: {"opcion": "EliminarProducto",id:id},
                        success: function(result){
                            if(result=='Ok'){
                                Swal.fire({
                                    icon: 'success',
                                    text: 'Producto eliminado',
                                    onClose: () => {
                                        window.location = "products.php";
                                    }
                                });
                            }else{
                                Swal.fire({
                                    icon: 'error',
                                    text: result
                                });
                            }
                        }
                    });
                }
            });
    }
</script>
</body>
</html>