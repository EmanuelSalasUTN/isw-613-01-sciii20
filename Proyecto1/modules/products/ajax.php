<?php
	session_start();
    header('Content-type: application/json');
    require_once('../../include/dbconnection.php');
    $dbConnection = new DbConnection();
	switch ($_REQUEST['opcion']) {
        case 'EliminarProducto':
            $id = $_POST["id"];
            $query_delete = "DELETE FROM productos WHERE id = $id";
            $result = $dbConnection->getDbConnectionObject()->query($query_delete);
            if($result){
                $dbConnection->closeConnection();
                echo json_encode("Ok");
            }else{
                $dbConnection->closeConnection();
                echo json_encode("Error al eliminar");
            }
        break;
        case 'crearProducto':
            // upload image
            $target_file = "";
            if(strlen($_FILES["productImg"]["tmp_name"])>1){
                $file_tmp = $_FILES["productImg"]["tmp_name"];
                $target_dir = "images/";
                $target_file = $target_dir . basename($_FILES["productImg"]["name"]);
                move_uploaded_file($file_tmp,$target_file);
            }
            //insert product to db
            $query="Insert Into productos (`sku`,`nombre`,`descripcion`,`imagen`,`id_categoria`,`stock`,`precio`) values('{$_POST['sku']}','{$_POST['nombre']}','{$_POST['descripcion']}','{$target_file}','{$_POST['categoria']}','{$_POST['stock']}','{$_POST['precio']}')";
            $result = $dbConnection->getDbConnectionObject()->query($query);
            if($result){
                $dbConnection->closeConnection();
                echo json_encode("Ok");
            }else{
                $dbConnection->closeConnection();
                echo json_encode("Error al actualizar");
            }
        break;
        case 'editarProducto':
            $id = $_POST["id"];
            // upload image
            $target_file = "";
            if(strlen($_FILES["productImg"]["tmp_name"])>1){
                $file_tmp = $_FILES["productImg"]["tmp_name"];
                $target_dir = "images/";
                $target_file = $target_dir . basename($_FILES["productImg"]["name"]);
                move_uploaded_file($file_tmp,$target_file);
            }
            if(strlen($target_file)>3){
                $query= "UPDATE `productos` SET sku = '{$_POST['sku']}', nombre = '{$_POST['nombre']}',descripcion = '{$_POST['descripcion']}',imagen = '{$target_file}',id_categoria = '{$_POST['categoria']}',stock = '{$_POST['stock']}',precio = '{$_POST['precio']}'  WHERE id = '$id'";
            }else{
                $query= "UPDATE `productos` SET sku = '{$_POST['sku']}', nombre = '{$_POST['nombre']}',descripcion = '{$_POST['descripcion']}',id_categoria = '{$_POST['categoria']}',stock = '{$_POST['stock']}',precio = '{$_POST['precio']}'  WHERE id = '$id'";
            }
            $result = $dbConnection->getDbConnectionObject()->query($query);
            if($result){
                $dbConnection->closeConnection();
                echo json_encode("Ok");
            }else{
                $dbConnection->closeConnection();
                echo json_encode("Error al actualizar");
            }
        break;
	}
?>
