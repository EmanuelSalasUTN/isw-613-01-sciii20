<div class="sidebar sidebar-dark bg-dark">
    <ul class="list-unstyled">
        <?php
        if($user["role"] == "Administrador"){
        ?>
            <li><a href="/index.php"><i class="fa fa-fw fa-home"></i> Dashboard</a></li>
            <li><a href="/modules/categories/categories.php"><i class="fa fa-fw fa-list-alt"></i> Categorias</a></li>
            <li><a href="/modules/products/products.php"><i class="fa fa-fw fa-list"></i> Productos</a></li>
        <?php 
        }else{
        ?>
            <li><a href="/index.php"><i class="fa fa-fw fa-home"></i> Dashboard</a></li>
            <li><a href="/modules/client/catalog/categories.php"><i class="fa fa-fw fa-list-alt"></i> Catálogo de Productos</a></li>
            <li><a href="/modules/client/ordenes/ordenes.php"><i class="fa fa-fw fa-book"></i> Ordenes</a></li>
        <?php 
        }
        ?>
    </ul>
</div>