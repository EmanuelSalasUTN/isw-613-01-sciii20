<?php
    session_start();
    require_once('../../../include/dbconnection.php');
    $user = $_SESSION['user'];
    if (!$user) {
        header('Location: ../../modules/auth/login_form.php');
    }
    if($user["role"] != "Cliente"){
        header('Location: /index.php');
    }
    $dbConnection = new DbConnection();
    $id_cat = $_REQUEST["id"];
?>
<hr>
<?php
    //$queryCategorias = "SELECT id_padre as idpadre,(SELECT nombre FROM categorias WHERE id = idpadre) as nombre FROM categorias where id_padre >0 group by id_padre";
    $queryCategorias = "SELECT id,nombre FROM categorias where id_padre = $id_cat";
    $resultcat = $dbConnection->getDbConnectionObject()->query($queryCategorias);
    if(mysqli_num_rows($resultcat)>0){
        echo '<div class="card">
                <div class="card-header font-weight-bold">
                    Seleccionar Sub Categoria
                </div>
                <ul class="list-group list-group-flush">';
                    while($row = $resultcat->fetch_array()){
                        echo '<li id="subcatli'.$row["id"].'" style="cursor:pointer;" onclick="CargarProductos('.$row["id"].')" class="list-group-item lisubcat list-group-item-action">'.$row["nombre"].'</li>';
                    }
        echo '</ul>
            </div>';
        ?>
            <script>
                function CargarProductos(id){
                    //cargar subcategorias y productos
                    $(".lisubcat").removeClass("active");
                    $("#subcatli"+id).addClass("active");
                    $.blockUI({ message: '<h4>Cargando...</h4>' });
                    $("#contenido_products").load( "products.php", { id: id }, function() {
                        
                    });
                }
            </script>
        <?php
    }
?>
<script>
    $( document ).ready(function() {
        $.blockUI({ message: '<h4>Cargando...</h4>' });
        $("#contenido_products").load( "products.php", { id: '<?php echo $id_cat; ?>' }, function() {
            
        });
    });
</script>
    