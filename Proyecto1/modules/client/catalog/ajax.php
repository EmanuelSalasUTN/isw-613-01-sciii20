<?php
	session_start();
    header('Content-type: application/json');
    require_once('../../../include/dbconnection.php');
    $dbConnection = new DbConnection();
	switch ($_REQUEST['opcion']) {
        case 'agregar_carrito':
            $user = $_SESSION['user'];
            $cantidad = $_POST['cantidad'];
            $idproducto = $_POST['id'];
            $total = 0;
            //sacar total del producto
            $queryproducto = "SELECT precio,stock FROM productos WHERE id = $idproducto";
            $resultproducto = $dbConnection->getDbConnectionObject()->query($queryproducto);
            $producto = $resultproducto->fetch_array();
            if($producto["stock"] < $cantidad){
                $dbConnection->closeConnection();
                echo json_encode("Error al agregar al carrito, no hay stock suficiente");
                return;
            }
            $total = $cantidad * $producto["precio"];
            //se inserta registro al shopping cart
            $query="Insert Into shopping_cart (`id_user`,`id_producto`,`cantidad`,`total`) values('{$user['id']}','{$idproducto}','{$cantidad}','{$total}')";
            $result = $dbConnection->getDbConnectionObject()->query($query);
            if($result){
                $dbConnection->closeConnection();
                echo json_encode("Ok");
            }else{
                $dbConnection->closeConnection();
                echo json_encode("Error al agregar al carrito");
            }
        break;
	}
?>
