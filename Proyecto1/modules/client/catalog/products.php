<?php
    session_start();
    require_once('../../../include/dbconnection.php');
    $user = $_SESSION['user'];
    if (!$user) {
        header('Location: ../../modules/auth/login_form.php');
    }
    if($user["role"] != "Cliente"){
        header('Location: /index.php');
    }
    $dbConnection = new DbConnection();
    $id_cat = $_REQUEST["id"];
    //se obtienen las subcategorias
    $queryCategorias = "SELECT id,nombre FROM categorias where id_padre = $id_cat";
    $resultcat = $dbConnection->getDbConnectionObject()->query($queryCategorias);
    $categoriasarray = array();
    while($rowcat = $resultcat->fetch_array()){
        array_push($categoriasarray,$rowcat["id"]);
    }
    array_push($categoriasarray,$id_cat);
    //se obtienen los productos
    $sql = "SELECT productos.id,sku,productos.nombre as nombre_producto,descripcion,imagen,id_categoria,stock,precio,categorias.nombre as nombre_cat FROM productos INNER JOIN categorias ON productos.id_categoria = categorias.id WHERE id_categoria IN (".implode(",",$categoriasarray).")";
    $result = $dbConnection->getDbConnectionObject()->query($sql);
    echo "<hr><h4>Lista de Productos</h4><hr>";
    echo "<div class='row'>";
    if(mysqli_num_rows($result)>0){
        while($row = $result->fetch_array()){//se imprimen todos los productos
        ?>
            <div class="col-md-3">
                <div class="card">
                    <div class="card-header">
                        <?php echo $row["nombre_producto"]; ?>
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">Precio: $<?php echo $row["precio"]; ?></h5>
                        <p class="card-text">
                            <?php echo $row["descripcion"]; ?>
                            <?php 
                                $imagen = "images/img_default.jpg";
                                if(strlen($row["imagen"])>1){
                                    $imagen = $row["imagen"];
                                }
                            ?>
                            <br>
                            <div class="w-100 text-center">
                                <img src="/modules/products/<?php echo $imagen; ?>" class="text-center mt-2" style="width:100px;" alt="">
                            </div>
                        </p>
                        <div class="w-100 text-center">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal<?php echo $row["id"]; ?>">
                                Ver Producto
                            </button>
                        </div>
                        <div class="modal fade" id="modal<?php echo $row["id"]; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel"><?php echo $row["nombre_producto"]; ?></h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div><span class="font-weight-bold">SKU</span>: <?php echo $row["sku"]; ?></div>
                                    <div><span class="font-weight-bold">Nombre</span>: <?php echo $row["nombre_producto"]; ?></div>
                                    <div><span class="font-weight-bold">Descripción</span>: <?php echo $row["descripcion"]; ?></div>
                                    <div><span class="font-weight-bold">Categoria</span>: <?php echo $row["nombre_cat"]; ?></div>
                                    <div><span class="font-weight-bold">Stock</span>: <?php echo $row["stock"]; ?></div>
                                    <div><span class="font-weight-bold">Precio</span>: $<?php echo $row["precio"]; ?></div>
                                    <div class="w-100 text-center">
                                        <img src="/modules/products/<?php echo $imagen; ?>" class="text-center mt-2" style="width:100px;" alt="">
                                        <table width="100%" class="text-center mt-2">
                                            <tr>
                                                <td class="text-right">Cantidad <input style="width:70px;" value="1" min="1" id="cantidad<?php echo $row["id"]; ?>" type="number"></td>
                                                <td class="text-left"><button onclick="agregarCarrito('<?php echo $row["id"]; ?>')" class="btn btn-primary"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Agregar al carrito</button></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                </div>
                                </div>
                            </div>
                            </div>
                    </div>
                </div>
            </div>
        <?php
        }
        echo "</div>";
        ?>
            <script>
                function agregarCarrito(id){
                    var cantidad = $("#cantidad"+id).val();
                    if(cantidad>0){
                        jQuery.ajax({
                            url: "ajax.php",
                            method: 'POST',
                            async:true,
                            data: {"opcion": "agregar_carrito",id:id,cantidad:cantidad},
                            success: function(result){
                                if(result=='Ok'){
                                    Swal.fire({
                                        icon: 'success',
                                        text: 'Articulo agregado',
                                        onClose: () => {
                                            window.location = "../shopping_cart/shopping_cart.php";
                                        }
                                    });
                                }else{
                                    Swal.fire({
                                        icon: 'error',
                                        text: result
                                    });
                                }
                            }
                        });
                    }else{
                        Swal.fire({
                            icon: 'error',
                            text: "Ingrese una cantidad correcta"
                        });
                    }
                }
            </script>
        <?php
    }else{
        echo "<h5>No se encontraron productos</h5>";
    }
?>
<script>
    $(document).ready(function(){
        $.unblockUI();
    });
</script>
