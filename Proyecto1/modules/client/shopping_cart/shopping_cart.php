<?php
    session_start();
    require_once('../../../include/dbconnection.php');
    $user = $_SESSION['user'];
    if (!$user) {
        header('Location: ../modules/auth/login_form.php');
    }
    if($user["role"] != "Cliente"){
        header('Location: /index.php');
    }
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="/assets/css/bootadmin.min.css">
    <title>E-shop</title>
</head>
<body class="bg-light">

<?php include '../../../modules/template/navbar.php'; ?>

<div class="d-flex">
    <?php include '../../../modules/template/sidebar.php'; ?>
    <div class="content p-4">
        <?php
            //Obtener datos de dashboard
            $dbConnection = new DbConnection();
            $sql = "SELECT shopping_cart.id, productos.nombre, productos.imagen, cantidad, total FROM shopping_cart INNER JOIN productos ON shopping_cart.id_producto = productos.id WHERE id_user = ".$user['id']." ORDER BY id ASC";
            $result = $dbConnection->getDbConnectionObject()->query($sql);
            $dbConnection->closeConnection();
            $total = 0;
        ?>
            <h2 class="mb-4">Carrito de compras</h2>

            <div class="card mb-4">
                <div class="card-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                    <th scope="col" style="width:80px;"></th>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Cantidad</th>
                                    <th scope="col">Precio</th>
                                    <th scope="col">Eliminar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        while($carrito = $result->fetch_array()){
                                            $total = $total + $carrito['total'];
                                            echo "<tr>";
                                                $imagen = "images/img_default.jpg";
                                                if(strlen($carrito["imagen"])>1){
                                                    $imagen = $carrito["imagen"];
                                                }
                                                echo "<td><img src='/modules/products/".$imagen."' class='text-center' style='width:70px;height:50px;'></td>";
                                                echo "<td>".$carrito['nombre']."</td>";
                                                echo "<td>".$carrito['cantidad']."</td>";
                                                echo "<td>$".$carrito['total']."</td>";
                                                echo "<td><button onclick='EliminarItem(".$carrito['id'].")' class='btn btn-sm btn-danger'><i class='fa fa-trash'></i> </button></td>";
                                            echo "</tr>";
                                        }
                                    ?>
                                    <tr><td></td><td></td><td></td><td class="font-weight-bold">Total</td><td class="font-weight-bold">$<?php echo $total; ?></td></tr>
                                </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <button onclick="checkout()" class="btn btn-success"><i class="fas fa-check"></i> Checkout</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>

<script src="/assets/js/jquery.min.js"></script>
<script src="/assets/js/bootstrap.bundle.min.js"></script>
<script src="/assets/js/bootadmin.min.js"></script>
<script src="/assets/js/sweetalert2.js"></script>
<script src="/assets/js/jquery.blockUI.js"></script>
<script>
    //funcion para checkout del carrito
    function checkout(){
        $.blockUI({ message: '<h4>Procesando...</h4>' });
        jQuery.ajax({
            url: "ajax.php",
            method: 'POST',
            async:true,
            data: {"opcion": "checkout"},
            success: function(result){
                if(result=='Ok'){
                    $.unblockUI();
                    Swal.fire({
                        icon: 'success',
                        text: 'Checkout completado',
                        onClose: () => {
                            window.location = "/index.php";
                        }
                    });
                }else{
                    $.unblockUI();
                    Swal.fire({
                        icon: 'error',
                        text: result
                    });
                }
            }
        });
    }
    //funcion para eliminar items del carrito
    function EliminarItem(id){
        //Preguntar para borrar la categoria
        Swal.fire({
            title: 'Eliminar item del carrito, esta seguro?',
            text: "Confirmar!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Si, eliminar!'
            }).then((result) => {
                if (result.isConfirmed) {
                    jQuery.ajax({
                        url: "ajax.php",
                        method: 'POST',
                        async:true,
                        data: {"opcion": "EliminarItem",id:id},
                        success: function(result){
                            if(result=='Ok'){
                                Swal.fire({
                                    icon: 'success',
                                    text: 'Item eliminado',
                                    onClose: () => {
                                        window.location = "shopping_cart.php";
                                    }
                                });
                            }else{
                                Swal.fire({
                                    icon: 'error',
                                    text: result
                                });
                            }
                        }
                    });
                }
            });
    }
</script>
</body>
</html>