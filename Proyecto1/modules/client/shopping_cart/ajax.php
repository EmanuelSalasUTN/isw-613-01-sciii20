<?php
	session_start();
    header('Content-type: application/json');
    require_once('../../../include/dbconnection.php');
    $user = $_SESSION['user'];
    $dbConnection = new DbConnection();
	switch ($_REQUEST['opcion']) {
        case 'EliminarItem':
            $id = $_POST["id"];
            $query_delete = "DELETE FROM shopping_cart WHERE id = $id";
            $result = $dbConnection->getDbConnectionObject()->query($query_delete);
            if($result){
                $dbConnection->closeConnection();
                echo json_encode("Ok");
            }else{
                $dbConnection->closeConnection();
                echo json_encode("Error al eliminar");
            }
        break;
        case 'checkout':
            try {
                // inicia transaccion
                $dbConnection->getDbConnectionObject()->autocommit(false);
                $dbConnection->getDbConnectionObject()->begin_transaction(MYSQLI_TRANS_START_READ_WRITE);
                //INICIA CHECKOUT
                //recorrer items del usuario para hacer checkout
                $sql = "SELECT id, id_producto, cantidad, total FROM shopping_cart WHERE id_user = ".$user['id']." ORDER BY id ASC";
                $result = $dbConnection->getDbConnectionObject()->query($sql);
                $total = 0;
                if(mysqli_num_rows($result)>0){
                    //crear orden
                    $query_orden="Insert Into ordenes (`id_user`,`total`,`fecha`) values('".$user['id']."','0',now())";
                    $result_orden = $dbConnection->getDbConnectionObject()->query($query_orden);
                    $id_orden = mysqli_insert_id($dbConnection->getDbConnectionObject());
                    //recorrer items del carrito para agregar a orden
                    while($carrito = $result->fetch_array()){
                        $idproducto = $carrito['id_producto'];
                        $cantidad = $carrito['cantidad'];
                        //obtener info del producto
                        $queryproducto = "SELECT precio,stock,nombre FROM productos WHERE id = $idproducto";
                        $resultproducto = $dbConnection->getDbConnectionObject()->query($queryproducto);
                        $producto = $resultproducto->fetch_array();
                        if($producto["stock"] < $cantidad){
                            throw new Exception("Error al hacer checkout, no hay stock suficiente para producto ".$producto["nombre"]." - Stock Disponible".$producto["stock"]);
                        }
                        $total = $total + $carrito["total"];
                        $query_orden_detalle="Insert Into ordenes_detalles (`id_orden`,`id_producto`,`cantidad`,`total`) values('".$id_orden."','{$idproducto}','{$cantidad}','{$carrito['total']}')";
                        $result_detalle = $dbConnection->getDbConnectionObject()->query($query_orden_detalle);
                        if(!$result_detalle){
                            throw new Exception("Error al hacer checkout, error al agregar detalle");
                        }
                        //actualizar stock
                        $nuevostock = $producto["stock"] - $cantidad;
                        $querystock = "UPDATE `productos` SET stock = '{$nuevostock}'  WHERE id = '$idproducto'";
                        $resultstock = $dbConnection->getDbConnectionObject()->query($querystock);
                        if(!$resultstock){
                            throw new Exception("Error al hacer checkout, error al rebajar stock");
                        }
                    }
                    //se actualiza el total de la orden
                    $querytotal = "UPDATE `ordenes` SET total = '{$total}'  WHERE id = '$id_orden'";
                    $resulttotal = $dbConnection->getDbConnectionObject()->query($querytotal);
                    if(!$resulttotal){
                        throw new Exception("Error al hacer checkout, error al actualizar total de orden");
                    }
                    //una vez procesado todo el carrito se borran los items
                    $query_delete = "DELETE FROM shopping_cart WHERE id_user = ".$user['id'];
                    $result = $dbConnection->getDbConnectionObject()->query($query_delete);
                }else{
                    throw new Exception("El carrito no tiene items agregados");
                }
                //TERMINA CHECKOUT
                $dbConnection->getDbConnectionObject()->commit();
                $dbConnection->getDbConnectionObject()->autocommit(true);
                echo json_encode("Ok");
            }catch(Exception $e) {
                $dbConnection->getDbConnectionObject()->rollback();
                $dbConnection->closeConnection();
                echo json_encode($e->getMessage());
            }
        break;
	}
?>
