<?php
    session_start();
    require_once('../../../include/dbconnection.php');
    $user = $_SESSION['user'];
    if (!$user) {
        header('Location: ../modules/auth/login_form.php');
    }
    if($user["role"] != "Cliente"){
        header('Location: /index.php');
    }
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="/assets/css/bootadmin.min.css">
    <title>E-shop</title>
</head>
<body class="bg-light">

<?php include '../../../modules/template/navbar.php'; ?>

<div class="d-flex">
    <?php include '../../../modules/template/sidebar.php'; ?>
    <div class="content p-4">
        <?php
            //Obtener datos de dashboard
            $dbConnection = new DbConnection();
            $sql = "SELECT id, total, fecha FROM ordenes WHERE id_user = ".$user['id']." ORDER BY id ASC";
            //$sql = "SELECT shopping_cart.id, productos.nombre, productos.imagen, cantidad, total FROM shopping_cart INNER JOIN productos ON shopping_cart.id_producto = productos.id WHERE id_user = ".$user['id']." ORDER BY id ASC";
            $result = $dbConnection->getDbConnectionObject()->query($sql);
            $total = 0;
        ?>
            <h2 class="mb-4">Ordenes</h2>

            <div class="card mb-4">
                <div class="card-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                    <th scope="col"># Orden</th>
                                    <th scope="col">Fecha de compra</th>
                                    <th scope="col">Total de la orden</th>
                                    <th scope="col">Items</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        while($orden = $result->fetch_array()){
                                            echo "<tr>";
                                                echo "<td>".$orden['id']."</td>";
                                                echo "<td>".date("d M Y", strtotime($orden["fecha"]))."</td>";
                                                echo "<td>$".$orden['total']."</td>";
                                                ?>
                                                    <td>
                                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal<?php echo $orden["id"]; ?>">
                                                        Ver Detalle
                                                    </button>
                                                    <div class="modal fade" id="modal<?php echo $orden["id"]; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog modal-lg" role="document">
                                                            <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel">Detalles de orden #<?php echo $orden["id"]; ?></h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <table class="table table-striped">
                                                                    <thead>
                                                                        <tr>
                                                                            <th scope="col">Producto</th>
                                                                            <th scope="col">Descripcion</th>
                                                                            <th scope="col">Cantidad</th>
                                                                            <th scope="col">Total</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <?php
                                                                            //obtener datos detallados de la orden
                                                                            $detalle = "SELECT productos.nombre, productos.descripcion, ordenes_detalles.cantidad, ordenes_detalles.total FROM ordenes_detalles INNER JOIN productos ON ordenes_detalles.id_producto = productos.id WHERE id_orden = ".$orden['id']." ORDER BY ordenes_detalles.id ASC";
                                                                            $result_detalle = $dbConnection->getDbConnectionObject()->query($detalle);
                                                                            while($rowdetalle = $result_detalle->fetch_array()){
                                                                                echo "<tr>";
                                                                                    echo "<td>".$rowdetalle['nombre']."</td>";
                                                                                    echo "<td>".$rowdetalle['descripcion']."</td>";
                                                                                    echo "<td>".$rowdetalle['cantidad']."</td>";
                                                                                    echo "<td>$".$rowdetalle['total']."</td>";
                                                                                echo "</tr>";
                                                                            }
                                                                        ?>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                                            </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    </td>
                                                <?php
                                            echo "</tr>";
                                        }
                                    ?>
                                </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>
<?php $dbConnection->closeConnection(); ?>
<script src="/assets/js/jquery.min.js"></script>
<script src="/assets/js/bootstrap.bundle.min.js"></script>
<script src="/assets/js/bootadmin.min.js"></script>
<script src="/assets/js/sweetalert2.js"></script>
<script src="/assets/js/jquery.blockUI.js"></script>
<script>
    //funcion para checkout del carrito
    function checkout(){
        $.blockUI({ message: '<h4>Procesando...</h4>' });
        jQuery.ajax({
            url: "ajax.php",
            method: 'POST',
            async:true,
            data: {"opcion": "checkout"},
            success: function(result){
                if(result=='Ok'){
                    $.unblockUI();
                    Swal.fire({
                        icon: 'success',
                        text: 'Checkout completado',
                        onClose: () => {
                            window.location = "/index.php";
                        }
                    });
                }else{
                    $.unblockUI();
                    Swal.fire({
                        icon: 'error',
                        text: result
                    });
                }
            }
        });
    }
    //funcion para eliminar items del carrito
    function EliminarItem(id){
        //Preguntar para borrar la categoria
        Swal.fire({
            title: 'Eliminar item del carrito, esta seguro?',
            text: "Confirmar!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Si, eliminar!'
            }).then((result) => {
                if (result.isConfirmed) {
                    jQuery.ajax({
                        url: "ajax.php",
                        method: 'POST',
                        async:true,
                        data: {"opcion": "EliminarItem",id:id},
                        success: function(result){
                            if(result=='Ok'){
                                Swal.fire({
                                    icon: 'success',
                                    text: 'Item eliminado',
                                    onClose: () => {
                                        window.location = "shopping_cart.php";
                                    }
                                });
                            }else{
                                Swal.fire({
                                    icon: 'error',
                                    text: result
                                });
                            }
                        }
                    });
                }
            });
    }
</script>
</body>
</html>