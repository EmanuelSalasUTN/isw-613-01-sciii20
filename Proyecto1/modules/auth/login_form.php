<html lang="en"><head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">

    <title>Login</title>

    <!-- Bootstrap core CSS -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/css/login.css" rel="stylesheet">
    <!-- Custom styles for this template -->
  </head>

  <body class="text-center">
    <div class="container">
      <?php
      if(isset($_REQUEST['msg_err'])){
        echo '
            <div class="row">
              <div class="col-md-12">
                <div class="alert alert-danger" role="alert">
                  '.$_REQUEST['msg_err'].'
                </div>
              </div>
            </div>';
      }
      if(isset($_REQUEST['msg'])){
        echo '
            <div class="row">
              <div class="col-md-12">
                <div class="alert alert-success" role="alert">
                  '.$_REQUEST['msg'].'
                </div>
              </div>
            </div>';
      }
      ?>
      <div class="row">
        <div class="col-md-12">
          <form method="post" action="login.php" class="form-signin">
            <img class="mb-4" src="/assets/img/store.png" alt="" width="72" height="72">
            <h1 class="h3 mb-3 font-weight-normal">Iniciar sesión</h1>
            <input type="text" id="inputUser" class="form-control" placeholder="Usuario" name="username" required="" autofocus="">
            <input type="password" id="inputPassword" class="form-control" placeholder="Password" name="password" required="">
            <button class="btn btn-lg btn-primary btn-block" type="submit">Iniciar sesión</button>
            <br>
            <a href="register_form.php">No tiene cuenta? Regístrese aquí</a>
            <p class="mt-5 mb-3 text-muted">© 2020</p>
          </form>
        </div>
      </div>
    </div>
</body></html>