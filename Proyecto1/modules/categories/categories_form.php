<?php
    session_start();
    require_once('../../include/dbconnection.php');
    $user = $_SESSION['user'];
    if (!$user) {
        header('Location: ../modules/auth/login_form.php');
    }
    if($user["role"] != "Administrador"){
        header('Location: /index.php');
    }
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="/assets/css/bootadmin.min.css">
    <title>E-shop</title>
</head>
<body class="bg-light">

<?php include '../../modules/template/navbar.php'; ?>

<div class="d-flex">
    <?php include '../../modules/template/sidebar.php'; ?>
    <div class="content p-4">
        <?php
            //Obtener datos de la categoria
            $dbConnection = new DbConnection();
            if(isset($_REQUEST["id"])){//verificar si viene el id de categoria
                $sql = "SELECT id,nombre,id_padre as idpadre,(SELECT nombre FROM categorias WHERE id = idpadre) as nombre_padre FROM categorias where id =".$_REQUEST['id'];
                $result = $dbConnection->getDbConnectionObject()->query($sql);
                if(mysqli_num_rows($result)==0){
                    $dbConnection->closeConnection();
                    header('Location: categories.php');
                }
                $categoria = $result->fetch_array();
            }else{
                $categoria = array("id"=>0,"nombre"=>"","idpadre"=>0);
            }
            
        ?>
        <h2 class="mb-4">Categorias</h2>
        <div class="card mb-4">
            <div class="card-header bg-white font-weight-bold">
                <?php
                    if(isset($_REQUEST["id"])){
                        echo 'Editar Categoria: '.$categoria["nombre"];
                    }else{
                        echo 'Crear Categoria';
                    }
                ?>
            </div>
            <div class="card-body">
                <form>
                    <div class="form-group">
                        <label for="inputNombre">Nombre</label>
                        <input type="text" class="form-control" id="inputNombre" value="<?php echo $categoria['nombre']; ?>" placeholder="Nombre">
                    </div>
                    <div class="form-group">
                        <label for="categoriaPadre">Categoria Padre</label>
                        <select class="form-control" name="categoriaPadre" id="categoriaPadre">
                            <option value="0">Seleccionar</option>
                            <?php 
                                $queryCategorias = "SELECT id,nombre FROM categorias";
                                $resultcat = $dbConnection->getDbConnectionObject()->query($queryCategorias);
                                while($row = $resultcat->fetch_array()){
                                    $select = "";
                                    if($row["id"] == $categoria["idpadre"]){
                                        $select = "selected";
                                    }
                                    echo "<option $select value='".$row["id"]."'>".$row["nombre"]."</option>";
                                }
                            ?>
                        </select>
                    </div>
                </form>
            </div>
            <div class="card-footer bg-white">
                <button onclick="GuardarCategoria('<?php echo $categoria['id']; ?>')" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </div>
</div>
<?php 
    $dbConnection->closeConnection();
?>
<script src="/assets/js/jquery.min.js"></script>
<script src="/assets/js/bootstrap.bundle.min.js"></script>
<script src="/assets/js/bootadmin.min.js"></script>
<script src="/assets/js/sweetalert2.js"></script>
<script>
    function GuardarCategoria(id){
        var opcion = "editarCategoria";
        if(id == "0"){
            opcion = "crearCategoria";
        }
        var nombre = $("#inputNombre").val();
        if(nombre.length == 0){
            Swal.fire({
                icon: 'error',
                text: "Debe ingresar un nombre"
            });
            return;
        }
        var cat_padre = $("#categoriaPadre").val();
        jQuery.ajax({
            url: "ajax.php",
            method: 'POST',
            async:true,
            data: {"opcion": opcion,id:id, nombre: nombre, cat_padre: cat_padre},
            success: function(result){
                if(result=='Ok'){
                    Swal.fire({
                        icon: 'success',
                        text: 'Categoria guardada',
                        onClose: () => {
                            window.location = "categories.php";
                        }
                    });
                }else{
                    Swal.fire({
                        icon: 'error',
                        text: result
                    });
                }
            }
        });
    }
</script>
</body>
</html>