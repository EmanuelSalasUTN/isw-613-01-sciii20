<?php
    session_start();
    require_once('../../include/dbconnection.php');
    $user = $_SESSION['user'];
    if (!$user) {
        header('Location: ../modules/auth/login_form.php');
    }
    if($user["role"] != "Administrador"){
        header('Location: /index.php');
    }
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="/assets/css/bootadmin.min.css">
    <title>E-shop</title>
</head>
<body class="bg-light">

<?php include '../../modules/template/navbar.php'; ?>

<div class="d-flex">
    <?php include '../../modules/template/sidebar.php'; ?>
    <div class="content p-4">
        <?php
            //Obtener datos de dashboard
            $dbConnection = new DbConnection();
            $sql = "SELECT id,nombre,id_padre as idpadre,(SELECT nombre FROM categorias WHERE id = idpadre) as nombre_padre FROM categorias";
            $result = $dbConnection->getDbConnectionObject()->query($sql);
            $dbConnection->closeConnection();
        ?>
            <h2 class="mb-4">Categorias</h2>

            <div class="card mb-4">
                <div class="card-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                            <table class="table">
                                <thead>
                                    <tr>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Categoria Padre</th>
                                    <th scope="col">Editar</th>
                                    <th scope="col">Eliminar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        while($categoria = $result->fetch_array()){
                                            echo "<tr>";
                                                echo "<td><span id='cat".$categoria['id']."'>".$categoria['nombre']."</span></td>";
                                                echo "<td>".$categoria['nombre_padre']."</td>";
                                                echo "<td><a href='categories_form.php?id=".$categoria['id']."'><button class='btn btn-primary'>Editar</button></a></td>";
                                                echo "<td><button class='btn btn-danger' onclick='EliminarCategory(".$categoria['id'].")'>Eliminar</button></td>";
                                            echo "</tr>";
                                        }
                                    ?>
                                </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <a href="categories_form.php"><button class="btn btn-success"><i class="fas fa-plus"></i> Nueva Categoria</button></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>

<script src="/assets/js/jquery.min.js"></script>
<script src="/assets/js/bootstrap.bundle.min.js"></script>
<script src="/assets/js/bootadmin.min.js"></script>
<script src="/assets/js/sweetalert2.js"></script>
<script>
    function EliminarCategory(id){
        //Preguntar para borrar la categoria
        Swal.fire({
            title: 'Eliminar categoria '+$("#cat"+id).html()+', esta seguro?',
            text: "Esto no se puede revertir!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Si, eliminar!'
            }).then((result) => {
                if (result.isConfirmed) {
                    jQuery.ajax({
                        url: "ajax.php",
                        method: 'POST',
                        async:true,
                        data: {"opcion": "EliminarCat",id:id},
                        success: function(result){
                            if(result=='Ok'){
                                Swal.fire({
                                    icon: 'success',
                                    text: 'Categoria eliminada',
                                    onClose: () => {
                                        window.location = "categories.php";
                                    }
                                });
                            }else{
                                Swal.fire({
                                    icon: 'error',
                                    text: result
                                });
                            }
                        }
                    });
                }
            });
    }
</script>
</body>
</html>