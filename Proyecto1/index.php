<?php
    session_start();
    require_once('include/dbconnection.php');
    $user = $_SESSION['user'];
    if (!$user) {
        header('Location: modules/auth/login_form.php');
    }
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="assets/css/bootadmin.min.css">
    <title>E-shop</title>
</head>
<body class="bg-light">

<?php include 'modules/template/navbar.php'; ?>

<div class="d-flex">
    <?php include 'modules/template/sidebar.php'; ?>
    <div class="content p-4">
        <?php
            if($user["role"] == "Administrador"){ //Mostrar Dashboard para usuario administrador
                //Obtener datos de dashboard
                $dbConnection = new DbConnection();
                $sql = "SELECT * FROM users";
                $result = $dbConnection->getDbConnectionObject()->query($sql);
                $totalUsuarios = mysqli_num_rows($result);
                //obtener datos de ventas
                $sqlventas = "SELECT sum(ordenes_detalles.cantidad) as productos, sum(ordenes_detalles.total) as montoadquirido  FROM ordenes_detalles";
                $resultventas = $dbConnection->getDbConnectionObject()->query($sqlventas);
                $totalcompras = 0;
                $totaladquirido = 0;
                if(mysqli_num_rows($resultventas)>0){
                    $compra = $resultventas->fetch_array();
                    $totalcompras = $compra["productos"];
                    $totaladquirido = $compra["montoadquirido"];
                }
                $dbConnection->closeConnection();
        ?>
                <h2 class="mb-4">Dashboard</h2>

                <div class="card mb-4">
                    <div class="card-body">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md">
                                    <div class="d-flex border">
                                        <div class="bg-info text-light p-4">
                                            <div class="d-flex align-items-center h-100">
                                                <i class="fa fa-3x fa-fw fa-users"></i>
                                            </div>
                                        </div>
                                        <div class="flex-grow-1 bg-white p-4">
                                            <p class="text-uppercase text-secondary mb-0">Clientes Registrados</p>
                                            <h3 class="font-weight-bold mb-0"><?php echo $totalUsuarios; ?></h3>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md">
                                    <div class="d-flex border">
                                        <div class="bg-primary text-light p-4">
                                            <div class="d-flex align-items-center h-100">
                                                <i class="fa fa-3x fa-fw fa-check"></i>
                                            </div>
                                        </div>
                                        <div class="flex-grow-1 bg-white p-4">
                                            <p class="text-uppercase text-secondary mb-0">Cantidad Productos Vendidos</p>
                                            <h3 class="font-weight-bold mb-0"><?php echo $totalcompras; ?></h3>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md">
                                    <div class="d-flex border">
                                        <div class="bg-success text-light p-4">
                                            <div class="d-flex align-items-center h-100">
                                                <i class="fa fa-3x fa-fw fa-dollar-sign"></i>
                                            </div>
                                        </div>
                                        <div class="flex-grow-1 bg-white p-4">
                                            <p class="text-uppercase text-secondary mb-0">Monto Total de Ventas</p>
                                            <h3 class="font-weight-bold mb-0"><?php echo $totaladquirido; ?></h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        <?php
            }else{
                $dbConnection = new DbConnection();
                $sql = "SELECT sum(ordenes_detalles.cantidad) as productos, sum(ordenes_detalles.total) as montoadquirido  FROM ordenes INNER JOIN ordenes_detalles ON ordenes.id = ordenes_detalles.id_orden WHERE ordenes.id_user = ".$user["id"];
                $result = $dbConnection->getDbConnectionObject()->query($sql);
                $totalcompras = 0;
                $totaladquirido = 0;
                if(mysqli_num_rows($result)>0){
                    $compra = $result->fetch_array();
                    if($compra["productos"]>0){
                        $totalcompras = $compra["productos"];
                    }
                    if($compra["montoadquirido"]>0){
                        $totaladquirido = $compra["montoadquirido"];
                    }
                }
                $dbConnection->closeConnection();
                ?>
                    <h2 class="mb-4">Dashboard</h2>
                    <div class="card mb-4">
                        <div class="card-body">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-md">
                                        <div class="d-flex border">
                                            <div class="bg-primary text-light p-4">
                                                <div class="d-flex align-items-center h-100">
                                                    <i class="fa fa-3x fa-fw fa-check"></i>
                                                </div>
                                            </div>
                                            <div class="flex-grow-1 bg-white p-4">
                                                <p class="text-uppercase text-secondary mb-0">Total Articulos Adquiridos</p>
                                                <h3 class="font-weight-bold mb-0"><?php echo $totalcompras; ?></h3>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md">
                                        <div class="d-flex border">
                                            <div class="bg-success text-light p-4">
                                                <div class="d-flex align-items-center h-100">
                                                    <i class="fa fa-3x fa-fw fa-dollar-sign"></i>
                                                </div>
                                            </div>
                                            <div class="flex-grow-1 bg-white p-4">
                                                <p class="text-uppercase text-secondary mb-0">Monto Total Adquirido</p>
                                                <h3 class="font-weight-bold mb-0">$<?php echo $totaladquirido; ?></h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php
            }
        ?>
    </div>
</div>

<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/bootstrap.bundle.min.js"></script>
<script src="assets/js/bootadmin.min.js"></script>

</body>
</html>