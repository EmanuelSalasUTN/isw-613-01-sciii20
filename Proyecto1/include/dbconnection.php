<?php

class DbConnection  {

  //DB connection info
  const MYSQL_PORT = 3306;
  private $dbConnectionObject,$user,$password,$host,$databaseName;

  public function __construct() {
    $this->user = "root";
    $this->password = "";
    $this->host = "localhost";
    $this->databaseName = "proyecto1";
    $this->dbConnectionObject = new mysqli($this->host.':'.self::MYSQL_PORT, $this->user, $this->password, $this->databaseName);
  }
  /**
   * Gets the existing MySQL DB Connection Object
   */
  public function getDbConnectionObject() {
    return $this->dbConnectionObject;
  }

  /**
   * Close the existing Db connection
   */
  public function closeConnection() {
    $this->dbConnectionObject->close();
  }

}