CREATE SCHEMA `proyecto1` ;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `lastname` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `role` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;
ALTER TABLE `proyecto1`.`users` 
ADD COLUMN `telefono` VARCHAR(45) NULL AFTER `lastname`,
ADD COLUMN `email` VARCHAR(55) NULL AFTER `telefono`,
ADD COLUMN `direccion` VARCHAR(100) NULL AFTER `email`;

CREATE TABLE `proyecto1`.`categorias` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NULL,
  `id_padre` INT NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`));

CREATE TABLE `proyecto1`.`productos` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `sku` VARCHAR(45) NOT NULL,
  `nombre` VARCHAR(145) NOT NULL,
  `descripcion` VARCHAR(245) NULL,
  `imagen` VARCHAR(245) NULL,
  `id_categoria` INT NOT NULL,
  `stock` INT(11) NOT NULL DEFAULT '0',
  `precio` DECIMAL(10,2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`));
  
ALTER TABLE `proyecto1`.`productos` 
CHANGE COLUMN `id_categoria` `id_categoria` INT(11) NOT NULL DEFAULT '0' ;

ALTER TABLE `proyecto1`.`productos` 
ADD CONSTRAINT `fk_Cat`
  FOREIGN KEY (`id_categoria`)
  REFERENCES `proyecto1`.`categorias` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
  
  CREATE TABLE `shopping_cart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `total` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


