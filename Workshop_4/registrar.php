<?php
    require('functions.php');
    if($_POST){
        $nuevocliente = saveClient(array("username"=>$_REQUEST['username'],"name"=>$_REQUEST['name'],"lastname"=>$_REQUEST['lastname'],"password"=>$_REQUEST['password']));
        if($nuevocliente){
            header('Location: dashboard.php?status=success&message=Client created successfully');
        }else{
            header('Location: dashboard.php?status=danger&message=Error creating client');
        }
    }else{
        header('Location: dashboard.php?status=danger&message=There was an error');
    }