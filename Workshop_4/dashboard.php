<?php
    session_start();

    $user = $_SESSION['user'];
    if (!$user) {
        header('Location: index.php');
    }
    $message = "";
    $status = "";
    if(!empty($_REQUEST['status'])) {
        $message = $_REQUEST['message'];
        $status = $_REQUEST['status'];
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

    <title>Document</title>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
            <div class="p-3 mb-2 text-white bg-<?php echo $status; ?>">
                <?php echo $message; ?>
            </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <h1> Bienvenido <?php echo $user['name']." ".$user['lastname'] ?> </h1>
            </div>
            <div class="col-md-4 mt-2">
                <button class="btn btn-light"><a href="logout.php">Logout</a></button> 
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?php  
                    if($user['role'] === 'Administrador') { ?>
                        <br><br>
                        <h5>Crear cliente</h5>
                        <br>
                        <form action="registrar.php" method="POST" role="form">
                            <div class="form-group">
                                <label for="user">User</label>
                                <input type="text" class="form-control" name="username" placeholder="Enter user">
                            </div>
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input type="password" class="form-control" name="password" placeholder="Password">
                            </div>
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" name="name" placeholder="Name">
                            </div>
                            <div class="form-group">
                                <label for="lastname">Last Name</label>
                                <input type="text" class="form-control" name="lastname" placeholder="Last Name">
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                <?php 
                    } ?>
            </div>
        </div>
</div>

</body>
</html>