<div class="sidebar sidebar-dark bg-dark">
    <ul class="list-unstyled">
		<?php
		$user = $this->session->user;
        if($user->role == "Administrador"){
        ?>
            <li><a href="/dashboard"><i class="fa fa-fw fa-home"></i> Dashboard</a></li>
            <li><a href="/categories"><i class="fa fa-fw fa-list-alt"></i> Categorias</a></li>
            <li><a href="/products"><i class="fa fa-fw fa-list"></i> Productos</a></li>
        <?php 
        }else{
        ?>
            <li><a href="/dashboard"><i class="fa fa-fw fa-home"></i> Dashboard</a></li>
            <li><a href="/catalog"><i class="fa fa-fw fa-list-alt"></i> Catálogo de Productos</a></li>
            <li><a href="/orders"><i class="fa fa-fw fa-book"></i> Ordenes</a></li>
        <?php 
        }
        ?>
    </ul>
</div>
