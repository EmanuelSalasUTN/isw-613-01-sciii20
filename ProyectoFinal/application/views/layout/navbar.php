<?php 
	$user = $this->session->user;
?>
<nav class="navbar navbar-expand navbar-dark bg-primary">
    <a class="sidebar-toggle mr-3" href="#"><i class="fa fa-bars"></i></a>
    <a class="navbar-brand" href="#">E-Shop</a>

    <div class="navbar-collapse collapse">
        <ul class="navbar-nav ml-auto">
            <?php
            //if($user["role"] == "Cliente"){
                //$dbConnection = new DbConnection();
               // $usuario =  $user['id'];
               // $sql = "SELECT * FROM shopping_cart WHERE id_user = $usuario";
               // $result = $dbConnection->getDbConnectionObject()->query($sql);
               // $totalCarrito = mysqli_num_rows($result);
            ?>
                <li class="nav-item"><a href="/shopping_cart" class="nav-link">Carrito Compras<i class="fas fa-shopping-cart"></i> </a></li>
            <?php 
           // }
            ?>
            <li class="nav-item dropdown">
                <a href="#" id="dd_user" class="nav-link dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $user->username; ?></a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dd_user">
                    <a href="/logout" class="dropdown-item">Logout</a>
                </div>
            </li>
        </ul>
    </div>
</nav>
