<?php 
	$user = $this->session->user;
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="/assets/css/bootadmin.min.css">
    <title>E-shop</title>
</head>
<body class="bg-light">

<?php $this->view('layout/navbar'); ?>

<div class="d-flex">
	<?php $this->view('layout/sidebar'); ?>
    <div class="content p-4">
        <h2 class="mb-4">Categorias</h2>
        <div class="card mb-4">
            <div class="card-header bg-white font-weight-bold">
                <?php
                    if($id>0){
                        echo 'Editar Categoria: '.$categoria->nombre;
                    }else{
                        echo 'Crear Categoria';
                    }
                ?>
            </div>
            <div class="card-body">
                <form>
                    <div class="form-group">
                        <label for="inputNombre">Nombre</label>
                        <input type="text" class="form-control" id="inputNombre" value="<?php echo $categoria->nombre; ?>" placeholder="Nombre">
                    </div>
                    <div class="form-group">
                        <label for="categoriaPadre">Categoria Padre</label>
                        <select class="form-control" name="categoriaPadre" id="categoriaPadre">
                            <option value="0">Seleccionar</option>
							<?php 
                                foreach ($categorias as $cat) {
                                    $select = "";
                                    if($cat->id == $categoria->idpadre){
                                        $select = "selected";
                                    }
                                    echo "<option $select value='".$cat->id."'>".$cat->nombre."</option>";
                                }
                            ?>
                        </select>
                    </div>
                </form>
            </div>
            <div class="card-footer bg-white">
                <button onclick="GuardarCategoria('<?php echo $categoria->id; ?>')" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </div>
</div>
<script src="/assets/js/jquery.min.js"></script>
<script src="/assets/js/bootstrap.bundle.min.js"></script>
<script src="/assets/js/bootadmin.min.js"></script>
<script src="/assets/js/sweetalert2.js"></script>
<script>
    function GuardarCategoria(id){
        var ruta = "/categories/update";
        if(id == "0"){
            ruta = "/categories/create";
        }
        var nombre = $("#inputNombre").val();
        if(nombre.length == 0){
            Swal.fire({
                icon: 'error',
                text: "Debe ingresar un nombre"
            });
            return;
        }
        var cat_padre = $("#categoriaPadre").val();
        jQuery.ajax({
            url: ruta,
            method: 'POST',
            async:true,
            data: {id:id, nombre: nombre, cat_padre: cat_padre},
            success: function(result){
                if(result=='Ok'){
                    Swal.fire({
                        icon: 'success',
                        text: 'Categoria guardada',
                        onClose: () => {
                            window.location = "/categories";
                        }
                    });
                }else{
                    Swal.fire({
                        icon: 'error',
                        text: result
                    });
                }
            }
        });
    }
</script>
</body>
</html>
