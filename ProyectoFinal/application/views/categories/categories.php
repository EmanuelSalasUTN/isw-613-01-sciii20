<?php 
	$user = $this->session->user;
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="/assets/css/bootadmin.min.css">
    <title>E-shop</title>
</head>
<body class="bg-light">

<?php $this->view('layout/navbar'); ?>

<div class="d-flex">
	<?php $this->view('layout/sidebar'); ?>
    <div class="content p-4">
		<h2 class="mb-4">Categorias</h2>

		<div class="card mb-4">
			<div class="card-body">
				<div class="container-fluid">
					<?php
						if(strlen($this->session->flashdata('msg'))>0){
							echo '<div class="row">
								<div class="col-md-12">
									<div class="alert alert-'.$this->session->flashdata('tipomsg').'" role="alert">
									'.$this->session->flashdata('msg').'
									</div>
								</div>
								</div>';
						}
					?>
					<div class="row">
						<div class="col-md-12">
						<table class="table">
							<thead>
								<tr>
								<th scope="col">Nombre</th>
								<th scope="col">Categoria Padre</th>
								<th scope="col">Editar</th>
								<th scope="col">Eliminar</th>
								</tr>
							</thead>
							<tbody>
								<?php
									foreach ($categorias as $categoria) {
										echo "<tr>";
											echo "<td><span id='cat".$categoria->id."'>".$categoria->nombre."</span></td>";
											echo "<td>".$categoria->nombre_padre."</td>";
											echo "<td><a href='/categories/update_form/".$categoria->id."'><button class='btn btn-primary'>Editar</button></a></td>";
											echo "<td><button class='btn btn-danger' onclick='EliminarCategory(".$categoria->id.")'>Eliminar</button></td>";
										echo "</tr>";
									}
								?>
							</tbody>
							</table>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<a href="/categories/create_form"><button class="btn btn-success"><i class="fas fa-plus"></i> Nueva Categoria</button></a>
						</div>
					</div>
				</div>
			</div>
		</div>
    </div>
</div>

<script src="/assets/js/jquery.min.js"></script>
<script src="/assets/js/bootstrap.bundle.min.js"></script>
<script src="/assets/js/bootadmin.min.js"></script>
<script src="/assets/js/sweetalert2.js"></script>
<script>
    function EliminarCategory(id){
        //Preguntar para borrar la categoria
        Swal.fire({
            title: 'Eliminar categoria '+$("#cat"+id).html()+', esta seguro?',
            text: "Esto no se puede revertir!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Si, eliminar!'
            }).then((result) => {
                if (result.isConfirmed) {
                    jQuery.ajax({
                        url: "categories/delete",
                        method: 'POST',
                        async:true,
                        data: {id:id},
                        success: function(result){
                            if(result=='Ok'){
                                Swal.fire({
                                    icon: 'success',
                                    text: 'Categoria eliminada',
                                    onClose: () => {
                                        window.location = "/categories";
                                    }
                                });
                            }else{
                                Swal.fire({
                                    icon: 'error',
                                    text: result
                                });
                            }
                        }
                    });
                }
            });
    }
</script>
</body>
</html>
