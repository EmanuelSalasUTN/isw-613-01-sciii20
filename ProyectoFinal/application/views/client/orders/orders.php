<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="/assets/css/bootadmin.min.css">
    <title>E-shop</title>
</head>
<body class="bg-light">

<?php $this->view('layout/navbar'); ?>

<div class="d-flex">
	<?php $this->view('layout/sidebar'); ?>
    <div class="content p-4">
        <?php
            $total = 0;
        ?>
            <h2 class="mb-4">Ordenes</h2>

            <div class="card mb-4">
                <div class="card-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                    <th scope="col"># Orden</th>
                                    <th scope="col">Fecha de compra</th>
                                    <th scope="col">Total de la orden</th>
                                    <th scope="col">Items</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        foreach ($ordenes as $orden) {
                                            echo "<tr>";
                                                echo "<td>".$orden->id."</td>";
                                                echo "<td>".date("d M Y", strtotime($orden->fecha))."</td>";
                                                echo "<td>$".$orden->total."</td>";
                                                ?>
                                                    <td>
                                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal<?php echo $orden->id; ?>">
                                                        Ver Detalle
                                                    </button>
                                                    <div class="modal fade" id="modal<?php echo $orden->id; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog modal-lg" role="document">
                                                            <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel">Detalles de orden #<?php echo $orden->id; ?></h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <table class="table table-striped">
                                                                    <thead>
                                                                        <tr>
                                                                            <th scope="col">Producto</th>
                                                                            <th scope="col">Descripcion</th>
                                                                            <th scope="col">Cantidad</th>
                                                                            <th scope="col">Total</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <?php
                                                                            //obtener datos detallados de la orden
                                                                            foreach ($orden->detalle as $rowdetalle) {
                                                                                echo "<tr>";
                                                                                    echo "<td>".$rowdetalle->nombre."</td>";
                                                                                    echo "<td>".$rowdetalle->descripcion."</td>";
                                                                                    echo "<td>".$rowdetalle->cantidad."</td>";
                                                                                    echo "<td>$".$rowdetalle->total."</td>";
                                                                                echo "</tr>";
                                                                            }
                                                                        ?>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                                            </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    </td>
                                                <?php
                                            echo "</tr>";
                                        }
                                    ?>
                                </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>
<script src="/assets/js/jquery.min.js"></script>
<script src="/assets/js/bootstrap.bundle.min.js"></script>
<script src="/assets/js/bootadmin.min.js"></script>
<script src="/assets/js/sweetalert2.js"></script>
<script src="/assets/js/jquery.blockUI.js"></script>
</body>
</html>
