<?php 
	$user = $this->session->user;
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="/assets/css/bootadmin.min.css">
    <title>E-shop</title>
</head>
<body class="bg-light">

<?php $this->view('layout/navbar'); ?>

<div class="d-flex">
	<?php $this->view('layout/sidebar'); ?>
    <div class="content p-4">
        <h2 class="mb-4">Categorias</h2>
        <div class="card mb-4">
            <div class="card-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header font-weight-bold">
                                    Seleccionar Categoria
                                </div>
                                <ul class="list-group list-group-flush">
                                    <?php 
                                        foreach ($categorias as $cat) {
                                            echo '<li id="licat'.$cat->id.'" style="cursor:pointer;" onclick="CargarSubCategoria('.$cat->id.')" class="list-group-item licat list-group-item-action">'.$cat->nombre.'</li>';
                                        }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div id="contenido_sub_categories" class="col-md-12">

                        </div>
                    </div>
                    <div class="row">
                        <div id="contenido_products" class="col-md-12">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/assets/js/jquery.min.js"></script>
<script src="/assets/js/bootstrap.bundle.min.js"></script>
<script src="/assets/js/bootadmin.min.js"></script>
<script src="/assets/js/sweetalert2.js"></script>
<script src="/assets/js/jquery.blockUI.js"></script>
<script>
    function CargarSubCategoria(id){
        $(".licat").removeClass("active");
        $("#licat"+id).addClass("active");
        //cargar subcategorias y productos
        $.blockUI({ message: '<h4>Cargando...</h4>' });
        $("#contenido_products").html("");
        $("#contenido_sub_categories").load( "/sub_categories", { id: id }, function() {
            
        });
    }
</script>
</body>
</html>
