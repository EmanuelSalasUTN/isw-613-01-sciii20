<?php
    $user = $this->session->user;
    $id_cat = $id;
?>
<hr>
<?php
    if(count($categorias)>0){
        echo '<div class="card">
                <div class="card-header font-weight-bold">
                    Seleccionar Sub Categoria
                </div>
                <ul class="list-group list-group-flush">';
                    foreach ($categorias as $cat) {
                        echo '<li id="subcatli'.$cat->id.'" style="cursor:pointer;" onclick="CargarProductos('.$cat->id.')" class="list-group-item lisubcat list-group-item-action">'.$cat->nombre.'</li>';
                    }
        echo '</ul>
            </div>';
        ?>
            <script>
                function CargarProductos(id){
                    //cargar subcategorias y productos
                    $(".lisubcat").removeClass("active");
                    $("#subcatli"+id).addClass("active");
                    $.blockUI({ message: '<h4>Cargando...</h4>' });
                    $("#contenido_products").load( "catalog/products", { id: id }, function() {
                        
                    });
                }
            </script>
        <?php
    }
?>
<script>
    $( document ).ready(function() {
        $.blockUI({ message: '<h4>Cargando...</h4>' });
        $("#contenido_products").load( "catalog/products", { id: '<?php echo $id_cat; ?>' }, function() {
            
        });
    });
</script>
    