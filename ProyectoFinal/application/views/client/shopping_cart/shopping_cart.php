<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="/assets/css/bootadmin.min.css">
    <title>E-shop</title>
</head>
<body class="bg-light">

<?php $this->view('layout/navbar'); ?>

<div class="d-flex">
	<?php $this->view('layout/sidebar'); ?>
    <div class="content p-4">
        <?php
            $total = 0;
        ?>
            <h2 class="mb-4">Carrito de compras</h2>

            <div class="card mb-4">
                <div class="card-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                    <th scope="col" style="width:80px;"></th>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Cantidad</th>
                                    <th scope="col">Precio</th>
                                    <th scope="col">Eliminar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        foreach ($items_carrito as $carrito) {
                                            $total = $total + $carrito->total;
                                            echo "<tr>";
                                                $imagen = "assets/images/img_default.jpg";
                                                if(strlen($carrito->imagen)>1){
                                                    $imagen = $carrito->imagen;
                                                }
                                                echo "<td><img src='/".$imagen."' class='text-center' style='width:70px;height:50px;'></td>";
                                                echo "<td>".$carrito->nombre."</td>";
                                                echo "<td>".$carrito->cantidad."</td>";
                                                echo "<td>$".$carrito->total."</td>";
                                                echo "<td><button onclick='EliminarItem(".$carrito->id.")' class='btn btn-sm btn-danger'><i class='fa fa-trash'></i> </button></td>";
                                            echo "</tr>";
                                        }
                                    ?>
                                    <tr><td></td><td></td><td></td><td class="font-weight-bold">Total</td><td class="font-weight-bold">$<?php echo $total; ?></td></tr>
                                </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <button onclick="checkout()" class="btn btn-success"><i class="fas fa-check"></i> Checkout</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>

<script src="/assets/js/jquery.min.js"></script>
<script src="/assets/js/bootstrap.bundle.min.js"></script>
<script src="/assets/js/bootadmin.min.js"></script>
<script src="/assets/js/sweetalert2.js"></script>
<script src="/assets/js/jquery.blockUI.js"></script>
<script>
    //funcion para checkout del carrito
    function checkout(){
        $.blockUI({ message: '<h4>Procesando...</h4>' });
        jQuery.ajax({
            url: "shopping_cart/checkout",
            method: 'POST',
            async:true,
            data: {"opcion": "checkout"},
            success: function(result){
                if(result=='Ok'){
                    $.unblockUI();
                    Swal.fire({
                        icon: 'success',
                        text: 'Checkout completado',
                        onClose: () => {
                            window.location = "/dashboard";
                        }
                    });
                }else{
                    $.unblockUI();
                    Swal.fire({
                        icon: 'error',
                        text: result
                    });
                }
            }
        });
    }
    //funcion para eliminar items del carrito
    function EliminarItem(id){
        //Preguntar para borrar la categoria
        Swal.fire({
            title: 'Eliminar item del carrito, esta seguro?',
            text: "Confirmar!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Si, eliminar!'
            }).then((result) => {
                if (result.isConfirmed) {
                    jQuery.ajax({
                        url: "shopping_cart/delete",
                        method: 'POST',
                        async:true,
                        data: {id:id},
                        success: function(result){
                            if(result=='Ok'){
                                Swal.fire({
                                    icon: 'success',
                                    text: 'Item eliminado',
                                    onClose: () => {
                                        window.location = "/shopping_cart";
                                    }
                                });
                            }else{
                                Swal.fire({
                                    icon: 'error',
                                    text: result
                                });
                            }
                        }
                    });
                }
            });
    }
</script>
</body>
</html>
