<html lang="en"><head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">

    <title>Registro</title>

    <!-- Bootstrap core CSS -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/css/login.css" rel="stylesheet">
    <!-- Custom styles for this template -->
  </head>

  <body class="text-center">
    <div class="container">
      <?php
      if(strlen($this->session->flashdata('msg'))>0){
        echo '<div class="row">
              <div class="col-md-12">
                <div class="alert alert-'.$this->session->flashdata('tipomsg').'" role="alert">
                  '.$this->session->flashdata('msg').'
                </div>
              </div>
            </div>';
      }
      ?>
      <div class="row">
        <div class="col-md-12">
          <form method="post" action="/register" class="form-signin text-left">
            <h1 class="h3 mb-3 text-center font-weight-normal">Registrarse</h1>
            <label for="inputNombre">Nombre</label>
            <input type="text" id="inputNombre" class="form-control" placeholder="Nombre" name="nombre" required="" autofocus="">
            <label for="inputApellidos">Apellidos</label>
            <input type="text" id="inputApellidos" class="form-control" placeholder="Apellidos" name="apellidos" required="" autofocus="">
            <label for="inputUsuario">Usuario</label>
            <input type="text" id="inputUsuario" class="form-control" placeholder="Usuario" name="username" required="" autofocus="">
            <label for="inputTel">Teléfono</label>
            <input type="text" id="inputTel" class="form-control" placeholder="Teléfono" name="telefono" required="" autofocus="">
            <label for="inputEmail">Email</label>
            <input type="email" id="inputEmail" class="form-control" placeholder="Email" name="email" required="" autofocus="">
            <label for="inputDireccion">Dirección</label>
            <input type="text" id="inputDireccion" class="form-control" placeholder="Dirección" name="direccion" required="" autofocus="">
            <label for="inputPassword">Contraseña</label>
            <input type="password" id="inputPassword" class="form-control" placeholder="Contraseña" name="password" required="">
            <button class="btn btn-lg btn-primary btn-block" type="submit">Registrarse</button>
            
            <p class="mt-5 mb-3 text-muted text-center">© 2020</p>
          </form>
        </div>
      </div>
    </div>
</body></html>
