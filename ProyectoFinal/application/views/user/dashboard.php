<?php 
$user = $this->session->user;
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="assets/css/bootadmin.min.css">
    <title>E-shop</title>
</head>
<body class="bg-light">

<?php $this->view('layout/navbar'); ?>

<div class="d-flex">
	<?php $this->view('layout/sidebar'); ?>
    <div class="content p-4">
		<h2 class="mb-4">Dashboard</h2>
		<div class="card mb-4">
			<div class="card-body">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md">
							<div class="d-flex border">
								<div class="bg-primary text-light p-4">
									<div class="d-flex align-items-center h-100">
										<i class="fa fa-3x fa-fw fa-check"></i>
									</div>
								</div>
								<div class="flex-grow-1 bg-white p-4">
									<p class="text-uppercase text-secondary mb-0">Total Articulos Adquiridos</p>
									<h3 class="font-weight-bold mb-0"><?php echo $totalcompras; ?></h3>
								</div>
							</div>
						</div>
						<div class="col-md">
							<div class="d-flex border">
								<div class="bg-success text-light p-4">
									<div class="d-flex align-items-center h-100">
										<i class="fa fa-3x fa-fw fa-dollar-sign"></i>
									</div>
								</div>
								<div class="flex-grow-1 bg-white p-4">
									<p class="text-uppercase text-secondary mb-0">Monto Total Adquirido</p>
									<h3 class="font-weight-bold mb-0">$<?php echo $totaladquirido; ?></h3>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
    </div>
</div>

<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/bootstrap.bundle.min.js"></script>
<script src="assets/js/bootadmin.min.js"></script>

</body>
</html>
