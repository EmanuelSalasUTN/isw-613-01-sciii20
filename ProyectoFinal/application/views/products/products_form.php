<?php 
	$user = $this->session->user;
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="/assets/css/bootadmin.min.css">
    <title>E-shop</title>
</head>
<body class="bg-light">

<?php $this->view('layout/navbar'); ?>

<div class="d-flex">
    <?php $this->view('layout/sidebar'); ?>
    <div class="content p-4">
        <h2 class="mb-4">Producto</h2>
        <div class="card mb-4">
            <div class="card-header bg-white font-weight-bold">
                <?php
                    if($id>0){
                        echo 'Editar Producto: '.$producto->nombre_producto;
                    }else{
                        echo 'Crear Producto';
                    }
                ?>
            </div>
            <div class="card-body">
                <form method="post" action="" enctype="multipart/form-data" id="formproduct">
                    <div class="form-group">
                        <label for="sku">SKU</label>
                        <input type="text" class="form-control" id="sku" name="sku" value="<?php echo $producto->sku; ?>" required="" placeholder="SKU">
                    </div>
                    <div class="form-group">
                        <label for="inputNombre">Nombre</label>
                        <input type="text" class="form-control" id="inputNombre" name="nombre" value="<?php echo $producto->nombre_producto; ?>" required="" placeholder="Nombre">
                    </div>
                    <div class="form-group">
                        <label for="descripcion">Descripcion</label>
                        <input type="text" class="form-control" id="descripcion" name="descripcion" value="<?php echo $producto->descripcion; ?>" placeholder="Descripcion">
                    </div>
                    <div class="form-group">
                        <label for="productImg">Imagen</label>
                        <input type="file" class="form-control" name="productImg" id="productImg">
                        <?php 
                            if(strlen($producto->imagen)>3){
                                echo "<img class='form-control' style='max-width:130px;' src='/".$producto->imagen."'>";
                            }
                        ?>
                    </div>
                    <div class="form-group">
                        <label for="categoriaPadre">Categoria</label>
                        <select class="form-control" name="categoria" id="categoria">
                            <option value="0">Seleccionar</option>
                            <?php
                                foreach ($categorias as $cat) {
                                    $select = "";
                                    if($cat->id == $producto->id_categoria){
                                        $select = "selected";
                                    }
                                    echo "<option $select value='".$cat->id."'>".$cat->nombre."</option>";
                                }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="stock">Stock</label>
                        <input type="number" min="0" required="" class="form-control" id="stock" name="stock" value="<?php echo $producto->stock; ?>" placeholder="Stock">
                    </div>
                    <div class="form-group">
                        <label for="precio">Precio $</label>
                        <input type="number" min="1" required="" step="any" class="form-control" id="precio" name="precio" value="<?php echo $producto->precio; ?>" placeholder="Precio">
                    </div>
                    <button type="submit" class="btn btn-success">Guardar</button>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="/assets/js/jquery.min.js"></script>
<script src="/assets/js/bootstrap.bundle.min.js"></script>
<script src="/assets/js/bootadmin.min.js"></script>
<script src="/assets/js/sweetalert2.js"></script>
<script>
    $('#formproduct').submit( function( e ) {
        e.preventDefault();
        var id = '<?php echo $producto->id; ?>';
        var ruta = "/products/update";
        if(id == "0"){
            ruta = "/products/create";
        }
        var categoria = $("#categoria").val();
        if(categoria == 0){
            Swal.fire({
                icon: 'error',
                text: "Debe seleccionar una categoria"
            });
            return;
        }
        var data = new FormData(this);
        data.append("id", id);
        $.ajax( {
            url: ruta,
            type: 'POST',
            data: data,
            processData: false,
            contentType: false,
            success: function(result){
                if(result=='Ok'){
                    Swal.fire({
                        icon: 'success',
                        text: 'Producto guardado',
                        onClose: () => {
                            window.location = "/products";
                        }
                    });
                }else{
                    Swal.fire({
                        icon: 'error',
                        text: result
                    });
                }
            }
        });
    } );
</script>
</body>
</html>
