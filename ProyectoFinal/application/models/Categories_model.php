<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categories_model extends CI_Model {

  /**
   *  Validate in the database that the user exists
   *
   * @param $username  The username
   * @param $password The user's password
   */
	public function getAllCategorias(){
		$this->db->select('id, nombre, id_padre as idpadre,(SELECT nombre FROM categorias WHERE id = idpadre) as nombre_padre');
		$this->db->from('categorias');
		$query = $this->db->get();
		if ($query->result()) {
			return $query->result();
		} else {
			return array();
		}
	}

	public function create($nombre,$idpadre){
		$this->db->insert('categorias', array('nombre' => $nombre, 'id_padre' => $idpadre));
		if($this->db->affected_rows() > 0) {
				return true;
		}else{
				return false;
		}
	}

	public function update($id,$nombre,$idpadre){
		$this->db->where('id', $id);
		$this->db->update('categorias', array('nombre' => $nombre, 'id_padre' => $idpadre));
		if ($this->db->affected_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public function delete($id){
		//verificar si tiene productos
		$this->db->select('id, nombre');
		$this->db->from('productos');
		$this->db->where('productos.id_categoria', $id);
		$query = $this->db->get();
		if ($query->num_rows()>0) {
			return "error";
		}else{
			$this->db->where('id', $id);
			$this->db->delete('categorias');
			$this->db->where('id_padre', $id);
			$filaseliminadas = $this->db->affected_rows();
			$this->db->update('categorias', array('id_padre' => 0));
			if($filaseliminadas > 0) {
				return "Ok";
			} else {
				return "Error desconocido";
			}
		}
	}

  /**
   *  Get user by Id
   *
   * @param $id  The user's id
   */
  public function getById($id){
		$this->db->select('id, nombre, id_padre as idpadre,(SELECT nombre FROM categorias WHERE id = idpadre) as nombre_padre');
		$this->db->from('categorias');
		$this->db->where('categorias.id', $id);
		$query = $this->db->get();
		if ($query->num_rows()>0) {
				return $query->result()[0];
		} else {
				return false;
		}
	}

	public function getByIdPadre($id_padre){
		$this->db->select('id,nombre');
		$this->db->from('categorias');
		$this->db->where('categorias.id_padre', $id_padre);
		$query = $this->db->get();
		if ($query->num_rows()>0) {
			return $query->result();
		} else {
			return array();
		}
	}

  /**
   *  Get all users from the databas
   *
   */
  public function all(){
      $query = $this->db->get('categorias');
      return $query->result();
	}
	

}
