<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shoppingcart_model extends CI_Model {

  /**
   *  Validate in the database that the user exists
   *
   * @param $username  The username
   * @param $password The user's password
   */
	public function getAllProductos(){
		$this->db->select('productos.id,sku,productos.nombre as nombre_producto,descripcion,imagen,id_categoria,stock,precio,categorias.nombre as nombre_cat');
		$this->db->join('categorias', 'categorias.id = productos.id_categoria');
		$this->db->from('productos');
		$query = $this->db->get();
		if ($query->result()) {
			return $query->result();
		} else {
			return array();
		}
	}

	public function create($iduser,$idproducto,$cantidad,$total){
		$this->db->insert('shopping_cart', array('id_user' => $iduser, 'id_producto' => $idproducto, 'cantidad' => $cantidad, 'total' => $total));
		if($this->db->affected_rows() > 0) {
			return true;
		}else{
			return false;
		}
	}

	public function getItemsByUser($iduser){
		$this->db->select('shopping_cart.id, productos.nombre, productos.imagen, cantidad, total');
		$this->db->from('shopping_cart');
		$this->db->join('productos', 'productos.id = shopping_cart.id_producto');
		$this->db->where('shopping_cart.id_user', $iduser);
		$this->db->order_by("shopping_cart.id", "asc");
		$query = $this->db->get();
		if ($query->result()) {
			return $query->result();
		} else {
			return array();
		}
	}

	public function delete($id){
		$this->db->where('id', $id);
		$this->db->delete('shopping_cart');
		if($this->db->affected_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}
	
	public function checkout($iduser){
		try {
			$this->db->trans_begin();
			//select items de usuario
			$this->db->select('id, id_producto, cantidad, total');
			$this->db->from('shopping_cart');
			$this->db->where('id_user', $iduser);
			$this->db->order_by("shopping_cart.id", "asc");
			$queryitems = $this->db->get();
			$total = 0;
			if ($queryitems->num_rows()>0) {
				//se inserta la orden
				$this->db->insert('ordenes', array('id_user' => $iduser, 'total' => '0', 'fecha' => date('Y-m-d H:i:s')));
				$id_orden = $this->db->insert_id();
				//se recorren items del carrito
				foreach($queryitems->result_array() as $carrito){
					$idproducto = $carrito['id_producto'];
					$cantidad = $carrito['cantidad'];
					//obtener info del producto
					$this->db->select('precio,stock,nombre');
					$this->db->from('productos');
					$this->db->where('id', $idproducto);
					$queryproducto = $this->db->get();
					$producto = $queryproducto->result()[0];
					if($producto->stock < $cantidad){
						throw new Exception("Error al hacer checkout, no hay stock suficiente para producto ".$producto->nombre." - Stock Disponible".$producto->stock);
					}
					$total = $total + $carrito["total"];
					//se agrega detalle de orden
					$this->db->insert('ordenes_detalles', array('id_orden' => $id_orden, 'id_producto' => $idproducto, 'cantidad' => $cantidad, 'total' => $carrito['total']));
					if($this->db->affected_rows() < 1){
						throw new Exception("Error al hacer checkout, error al agregar detalle");
					}
					//actualizar stock
					$nuevostock = $producto->stock - $cantidad;
					$this->db->where('id', $idproducto);
					$this->db->update('productos', array('stock' => $nuevostock));
					if($this->db->affected_rows() < 1) {
						throw new Exception("Error al hacer checkout, error al rebajar stock");
					}
				}
				//se actualiza el total de la orden
				$this->db->where('id', $id_orden);
				$this->db->update('ordenes', array('total' => $total));
				if($this->db->affected_rows() < 1) {
					throw new Exception("Error al hacer checkout, error al actualizar total de orden");
				}
				//una vez procesado todo el carrito se borran los items
				$this->db->where('id_user', $iduser);
				$this->db->delete('shopping_cart');
				if($this->db->affected_rows() < 1) {
					throw new Exception("Error al procesar carrito, intente nuevamente");
				}
			}else{
				throw new Exception("El carrito no tiene items agregados");
			}
			$this->db->trans_commit();
			$this->db->trans_complete();
			return "Ok";
		}catch(Exception $e) {
			$this->db->trans_rollback();
			return $e->getMessage();
		}
	}

  /**
   *  Get user by Id
   *
   * @param $id  The user's id
   */
  public function getOrderByUser($iduser){
		$this->db->select('id, total, fecha');
		$this->db->from('ordenes');
		$this->db->where('id_user', $iduser);
		$this->db->order_by("id", "asc");
		$query = $this->db->get();
		$array_ordenes = array();
		if ($query->num_rows()>0) {
			foreach($query->result_array() as $orden){
				$order = new stdClass;
				$order->id = $orden['id'];
				$order->total = $orden['total'];
				$order->fecha = $orden['fecha'];
				$order->detalle = array();
				//obtener detalles
				$this->db->select('productos.nombre, productos.descripcion, ordenes_detalles.cantidad, ordenes_detalles.total');
				$this->db->from('ordenes_detalles');
				$this->db->join('productos', 'productos.id = ordenes_detalles.id_producto');
				$this->db->where('id_orden', $orden['id']);
				$this->db->order_by("ordenes_detalles.id", "asc");
				$querydetalle = $this->db->get();
				foreach($querydetalle->result_array() as $detalle){
					$detail = new stdClass;
					$detail->nombre = $detalle['nombre'];
					$detail->descripcion = $detalle['descripcion'];
					$detail->cantidad = $detalle['cantidad'];
					$detail->total = $detalle['total'];
					array_push($order->detalle,$detail);
				}
				array_push($array_ordenes,$order);
			}
		}
		return $array_ordenes;
	}
	
}
