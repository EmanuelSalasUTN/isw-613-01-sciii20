<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_model extends CI_Model {

  /**
   *  Validate in the database that the user exists
   *
   * @param $username  The username
   * @param $password The user's password
   */
	public function getUserData($iduser){
		$this->db->select('sum(ordenes_detalles.cantidad) as productos,  sum(ordenes_detalles.total) as montoadquirido');
		$this->db->from('ordenes');
		$this->db->join('ordenes_detalles', 'ordenes_detalles.id_orden = ordenes.id');
		$this->db->where('ordenes.id_user', $iduser);
		$query = $this->db->get();
		if ($query->num_rows()>0) {
			return $query->result()[0];
		} else {
			return false;
		}
	}

	public function getVentasData(){
		$this->db->select('sum(ordenes_detalles.cantidad) as productos,  sum(ordenes_detalles.total) as montoadquirido ');
		$this->db->from('ordenes_detalles');
		$query = $this->db->get();
		if ($query->num_rows()>0) {
			return $query->result()[0];
		} else {
			return false;
		}
	}

  /**
   *  Get user by Id
   *
   * @param $id  The user's id
   */
	public function getById($id){
		$query = $this->db->get_where('users', array('id' => $id));
		if ($query->result()) {
			return $query->result();
		} else {
			return false;
		}
	}

  	public function all_users(){
		$query = $this->db->get('users');
		return $query->num_rows();
	}


}
