<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Productos_model extends CI_Model {

  /**
   *  Validate in the database that the user exists
   *
   * @param $username  The username
   * @param $password The user's password
   */
	public function getAllProductos(){
		$this->db->select('productos.id,sku,productos.nombre as nombre_producto,descripcion,imagen,id_categoria,stock,precio,categorias.nombre as nombre_cat');
		$this->db->join('categorias', 'categorias.id = productos.id_categoria');
		$this->db->from('productos');
		$query = $this->db->get();
		if ($query->result()) {
			return $query->result();
		} else {
			return array();
		}
	}

	public function create($sku,$nombre,$descripcion,$imagen,$idcategoria,$stock,$precio){
		$this->db->insert('productos', array('sku' => $sku, 'nombre' => $nombre, 'descripcion' => $descripcion, 'imagen' => $imagen, 'id_categoria' => $idcategoria, 'stock' => $stock, 'precio' => $precio));
		if($this->db->affected_rows() > 0) {
			return true;
		}else{
			return false;
		}
	}

	public function update($id,$sku,$nombre,$descripcion,$imagen,$idcategoria,$stock,$precio){
			$this->db->where('id', $id);
			if(strlen($imagen)>3){
				$this->db->update('productos', array('sku' => $sku, 'nombre' => $nombre, 'descripcion' => $descripcion, 'imagen' => $imagen, 'id_categoria' => $idcategoria, 'stock' => $stock, 'precio' => $precio));
			}else{
				$this->db->update('productos', array('sku' => $sku, 'nombre' => $nombre, 'descripcion' => $descripcion, 'id_categoria' => $idcategoria, 'stock' => $stock, 'precio' => $precio));
			}
			
			if ($this->db->affected_rows() > 0) {
				return true;
			} else {
				return false;
			}
	}

	public function delete($id){
			//verificar si tiene productos
			$this->db->where('id', $id);
			$this->db->delete('productos');
			if($this->db->affected_rows() > 0) {
				return true;
			} else {
				return false;
			}
	}

  /**
   *  Get user by Id
   *
   * @param $id  The user's id
   */
  public function getById($id){
		$this->db->select('productos.id,sku,productos.nombre as nombre_producto,descripcion,imagen,id_categoria,stock,precio,categorias.nombre as nombre_cat');
		$this->db->from('productos');
		$this->db->join('categorias', 'categorias.id = productos.id_categoria');
		$this->db->where('productos.id', $id);
		$query = $this->db->get();
		if ($query->num_rows()>0) {
			return $query->result()[0];
		} else {
			return false;
		}
	}

	public function getByCategorias($categoriasarray){
		//$sql = "SELECT productos.id,sku,productos.nombre as nombre_producto,descripcion,imagen,id_categoria,stock,precio,categorias.nombre as nombre_cat FROM productos INNER JOIN categorias ON productos.id_categoria = categorias.id WHERE id_categoria IN (".implode(",",$categoriasarray).")";
		$this->db->select('productos.id,sku,productos.nombre as nombre_producto,descripcion,imagen,id_categoria,stock,precio,categorias.nombre as nombre_cat');
		$this->db->from('productos');
		$this->db->join('categorias', 'categorias.id = productos.id_categoria');
		$this->db->where_in('id_categoria', $categoriasarray);
		$query = $this->db->get();
		if ($query->num_rows()>0) {
			return $query->result();
		} else {
			return array();
		}
	}

	public function getInfoById($id){
		$this->db->select('precio,stock');
		$this->db->from('productos');
		$this->db->where('productos.id', $id);
		$query = $this->db->get();
		if ($query->num_rows()>0) {
			return $query->result()[0];
		} else {
			return false;
		}
	}

  /**
   *  Get all users from the databas
   *
   */
  public function all(){
      $query = $this->db->get('productos');
      return $query->result();
	}
	

}
