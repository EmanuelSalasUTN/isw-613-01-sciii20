<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['dashboard'] = 'user/dashboard';
$route['ingresar'] = 'user/login';
$route['login'] = 'user/login';
$route['authenticate'] = 'user/authenticate';
$route['form_register'] = 'user/register_form';
$route['register'] = 'user/register';
$route['dashboard'] = 'welcome/index';
$route['logout'] = 'user/logout';
//categorias
$route['categories'] = 'categories/index';
$route['categories/create_form'] = 'categories/create_form';
$route["categories/update_form/(:num)"]="categories/create_form/$1";
$route['categories/create'] = 'categories/store';
$route['categories/update'] = 'categories/store';
$route['categories/delete'] = 'categories/delete';
//products
$route['products'] = 'products/index';
$route['products/create_form'] = 'products/create_form';
$route["products/update_form/(:num)"]="products/create_form/$1";
$route['products/create'] = 'products/store';
$route['products/update'] = 'products/store';
$route['products/delete'] = 'products/delete';
//rutas del cliente
$route['catalog'] = 'catalog/index';
$route['sub_categories'] = 'catalog/sub_categories';
$route['catalog/products'] = 'catalog/products';
$route['shopping_cart/add'] = 'shoppingcart/add';
$route['shopping_cart'] = 'shoppingcart/index';
$route['shopping_cart/delete'] = 'shoppingcart/delete';
$route['shopping_cart/checkout'] = 'shoppingcart/checkout';
$route['orders'] = 'shoppingcart/orders';

$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
