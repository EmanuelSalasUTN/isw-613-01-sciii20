<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends CI_Controller {

	public function index()
	{
		$this->load->model('Productos_model');
		if (!isset($_SESSION['user'])) {
			redirect(site_url('login'));
		}else{
			$user = $_SESSION['user'];
			if($user->role != "Administrador"){
				redirect(site_url('dashboard'));
			}else{
				//cargar datos de categorias
				$data['productos'] = $this->Productos_model->getAllProductos();
				$this->load->view('products/products',$data);
			}
		}
		
	}

	public function create_form($id = 0)
	{
		if (!isset($_SESSION['user'])) {
			redirect(site_url('login'));
		}else{
			$user = $_SESSION['user'];
			if($user->role != "Administrador"){
				redirect(site_url('dashboard'));
			}else{//si es administrador
				$this->load->model('Productos_model');
				$this->load->model('Categories_model');
				$productovacio = new stdClass;
				$productovacio->id = 0;
				$productovacio->sku = "";
				$productovacio->nombre_producto = "";
				$productovacio->descripcion = "";
				$productovacio->imagen = "";
				$productovacio->nombre_cat = "";
				$productovacio->id_categoria = 0;
				$productovacio->stock = 0;
				$productovacio->precio = 0;
				$data['producto'] = $productovacio;
				$data['id'] = 0;
				$data['productos'] = $this->Productos_model->all();
				$data['categorias'] = $this->Categories_model->all();
				if($id > 0){//verificar si viene el id de categoria
					$producto = $this->Productos_model->getById($id);
					$data['id'] = $id;
					if($producto){
						$data['producto'] = $producto;
					}else{
						redirect(site_url('products'));
					}
				}
				$this->load->view('products/products_form',$data);
			}
		}
	}

	public function store()
	{
		$this->load->model('Productos_model');
		$id = $this->input->post('id');
		$sku = $this->input->post('sku');
		$nombre = $this->input->post('nombre');
		$descripcion = $this->input->post('descripcion');
		$categoria = $this->input->post('categoria');
		$stock = $this->input->post('stock');
		$precio = $this->input->post('precio');
		$target_file = "";
		if(strlen($_FILES["productImg"]["tmp_name"])>1){
			$file_tmp = $_FILES["productImg"]["tmp_name"];
			$target_dir = "assets/images/";
			$target_file = $target_dir . basename($_FILES["productImg"]["name"]);
			move_uploaded_file($file_tmp,$target_file);
		}
		if($id == 0){//create
			$result = $this->Productos_model->create($sku,$nombre,$descripcion,$target_file,$categoria,$stock,$precio);
			if($result) {
				$this->session->set_flashdata('tipomsg', 'success');
				$this->session->set_flashdata('msg', 'Producto creado');
				echo 'Ok';
				return;
			} else {
				// send errors
				$this->session->set_flashdata('tipomsg', 'danger');
				$this->session->set_flashdata('msg', 'Error creando producto');
				echo "Error creando categoria";
				return;
			}
		}else{//update
			$result = $this->Productos_model->update($id,$sku,$nombre,$descripcion,$target_file,$categoria,$stock,$precio);
			if($result) {
				$this->session->set_flashdata('tipomsg', 'success');
				$this->session->set_flashdata('msg', 'Producto actualizado');
				echo "Ok";
				return;
			} else {
				// send errors
				$this->session->set_flashdata('tipomsg', 'danger');
				$this->session->set_flashdata('msg', 'Error actualizando producto');
				echo "Error actualizando producto";
				return;
			}
		}
	}

	public function delete()
	{
		$this->load->model('Productos_model');
		$id = $this->input->post('id');
		if($id > 0){
			$result = $this->Productos_model->delete($id);
			if($result) {
				$this->session->set_flashdata('tipomsg', 'success');
				$this->session->set_flashdata('msg', 'Producto eliminado');
				echo 'Ok';
				return;
			} else {
				// send errors
				$this->session->set_flashdata('tipomsg', 'danger');
				$this->session->set_flashdata('msg', 'Error eliminando producto');
				echo "Error eliminando producto";
				return;
			}
		}else{
			redirect(site_url('products'));
		}
	}
}
