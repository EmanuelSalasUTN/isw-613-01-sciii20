<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shoppingcart extends CI_Controller {

	public function index()
	{
		$this->load->model('Shoppingcart_model');
		if (!isset($_SESSION['user'])) {
			redirect(site_url('login'));
		}else{
			$user = $_SESSION['user'];
			if($user->role != "Cliente"){
				redirect(site_url('dashboard'));
			}else{
				//cargar datos de shopping cart
				$data['items_carrito'] = $this->Shoppingcart_model->getItemsByUser($user->id);
				$this->load->view('client/shopping_cart/shopping_cart',$data);
			}
		}
		
	}

	public function add()
	{
		$this->load->model('Shoppingcart_model');
		$this->load->model('Productos_model');
		$user = $_SESSION['user'];
		$idproducto = $this->input->post('id');
		$cantidad = $this->input->post('cantidad');
		if($idproducto > 0){//create
			$total = 0;
			//verificar stock
			$infoproducto = $this->Productos_model->getInfoById($idproducto);
			if($infoproducto->stock < $cantidad){
				echo "Error al agregar al carrito, no hay stock suficiente";
				return;
			}
			//sacar total del producto
			$total = $cantidad * $infoproducto->precio;
			$result = $this->Shoppingcart_model->create($user->id,$idproducto,$cantidad,$total);
			if($result) {
				echo 'Ok';
				return;
			} else {
				// send errors
				echo "Error creando categoria";
				return;
			}
		}
	}

	public function delete()
	{
		$this->load->model('Shoppingcart_model');
		$id = $this->input->post('id');
		if($id > 0){//delete
			$result = $this->Shoppingcart_model->delete($id);
			if($result) {
				echo 'Ok';
				return;
			} else {
				echo "Error eliminando item de carrito";
				return;
			}
		}else{
			redirect(site_url('shopping_cart'));
		}
	}
	
	public function checkout(){
		$user = $_SESSION['user'];
		$this->load->model('Shoppingcart_model');
		$checkout = $this->Shoppingcart_model->checkout($user->id);
		echo $checkout;
		return;
	}

	public function orders()
	{
		$this->load->model('Shoppingcart_model');
		if (!isset($_SESSION['user'])) {
			redirect(site_url('login'));
		}else{
			$user = $_SESSION['user'];
			if($user->role != "Cliente"){
				redirect(site_url('dashboard'));
			}else{
				//cargar datos de shopping cart
				$data['ordenes'] = $this->Shoppingcart_model->getOrderByUser($user->id);
				$this->load->view('client/orders/orders',$data);
			}
		}
		
	}
	
}
