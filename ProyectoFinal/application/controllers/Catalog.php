<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Catalog extends CI_Controller {

	public function index()
	{
		$this->load->model('Categories_model');
		if (!isset($_SESSION['user'])) {
			redirect(site_url('login'));
		}else{
			$user = $_SESSION['user'];
			if($user->role != "Cliente"){
				redirect(site_url('dashboard'));
			}else{
				//cargar datos de categorias
				$data['categorias'] = $this->Categories_model->all();
				$this->load->view('client/catalog/categories',$data);
			}
		}
		
	}

	public function sub_categories(){
		$this->load->model('Categories_model');
		$id = $_REQUEST['id'];
		$data['id'] = $id;
		$data['categorias'] = $this->Categories_model->getByIdPadre($id);
		$this->load->view('client/catalog/sub_categories',$data);
	}

	public function products(){
		$this->load->model('Productos_model');
		$this->load->model('Categories_model');
		$id_cat = $_REQUEST['id'];
		$data['id'] = $id_cat;
		$categorias = $this->Categories_model->getByIdPadre($id_cat);
		//se obtienen las subcategorias
		$categoriasarray = array();
		foreach ($categorias as $cat) {
			array_push($categoriasarray,$cat->id);
		}
		array_push($categoriasarray,$id_cat);
		$data['productos'] = $this->Productos_model->getByCategorias($categoriasarray);
		//se obtienen los productos
		$this->load->view('client/catalog/products',$data);
	}
	
}
