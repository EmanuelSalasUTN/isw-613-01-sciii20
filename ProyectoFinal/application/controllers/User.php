<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function register_form()
	{
		// $username = $this->input->get('u');
		// $password = $this->input->get('p');
		// $data['username'] = $username;
		// $data['password'] = $password;
		$this->load->view('user/register');
	}

	public function register()
	{
		$this->load->model('User_model');
		// read login params (user/pass)
		$username = $this->input->post('username');
		$nombre = $this->input->post('nombre');
		$apellidos = $this->input->post('apellidos');
		$telefono = $this->input->post('telefono');
		$email = $this->input->post('email');
		$direccion = $this->input->post('direccion');
		$pass = $this->input->post('password');
		$result = $this->User_model->insert($nombre,$username,$apellidos,$telefono,$email,$direccion,$pass);
		if($result) {
			$this->session->set_flashdata('tipomsg', 'success');
			$this->session->set_flashdata('msg', 'User created, please login');
			redirect(site_url('login'));
		} else {
			// send errors
			$this->session->set_flashdata('tipomsg', 'danger');
			$this->session->set_flashdata('msg', 'Error registrando');
			redirect(site_url('form_register'));
		}
	}

	public function login()
	{
    	if (!isset($_SESSION['user'])) {
			$this->load->view('user/login');
		}else{
			redirect(site_url('dashboard'));
		}
	}

	public function authenticate()
	{
		$this->load->model('User_model');
		// read login params (user/pass)
		$username = $this->input->post('username');
		$pass = $this->input->post('password');
		$user = $this->User_model->authenticate($username, $pass);
		if(!$user) {
			redirect(site_url('login'));
		} else {
            $_SESSION['user'] = $user;
			redirect(site_url('dashboard'));
		}
	}

	public function logout(){
		session_destroy();
    	redirect(site_url('dashboard'));
	}
}
