<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->model('Dashboard_model');
		//validar si esta logueado
		if (!isset($_SESSION['user'])) {
			redirect(site_url('login'));
		}else{
			$user = $_SESSION['user'];
			if($user->role != "Administrador"){
				//se cargan datos de cliente
				$datos = $this->Dashboard_model->getUserData($user->id);
				$data["totalcompras"] = 0;
				$data["totaladquirido"] = 0;
				if(isset($datos->productos)){
					if($datos->productos>0){
						$data["totalcompras"] = $datos->productos;
					}
					if($datos->montoadquirido>0){
						$data["totaladquirido"] = $datos->montoadquirido;
					}
				}
				$this->load->view('user/dashboard.php',$data);
			}else{
				//cargar datos de administrador
				$data['totalUsuarios'] = $this->Dashboard_model->all_users();
				//obtener datos de ventas
				$data['totalcompras'] = 0;
				$data['totaladquirido'] = 0;
				$datos = $this->Dashboard_model->getVentasData();
				if(isset($datos->productos)){
					$data['totalcompras'] = $datos->productos;
					$data['totaladquirido'] = $datos->montoadquirido;
				}
				$this->load->view('admin/dashboard.php',$data);
			}
		}
		//validacion admin
		//if (!isset($_SESSION['user'])) {
		//	redirect(site_url('login'));
		//}else{
		//	$user = $_SESSION['user'];
		//	if($user->role != "Administrador"){
		//		redirect(site_url('dashboard'));
		//	}else{//si es administrador
				
		//	}
		//}
	}
}
