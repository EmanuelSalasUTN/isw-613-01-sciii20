<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categories extends CI_Controller {

	public function index()
	{
		$this->load->model('Categories_model');
		if (!isset($_SESSION['user'])) {
			redirect(site_url('login'));
		}else{
			$user = $_SESSION['user'];
			if($user->role != "Administrador"){
				redirect(site_url('dashboard'));
			}else{
				//cargar datos de categorias
				$data['categorias'] = $this->Categories_model->getAllCategorias();
				$this->load->view('categories/categories',$data);
			}
		}
		
	}

	public function create_form($id = 0)
	{
		if (!isset($_SESSION['user'])) {
			redirect(site_url('login'));
		}else{
			$user = $_SESSION['user'];
			if($user->role != "Administrador"){
				redirect(site_url('dashboard'));
			}else{//si es administrador
				$this->load->model('Categories_model');
				$categoriavacia = new stdClass;
				$categoriavacia->id = 0;
				$categoriavacia->nombre = "";
				$categoriavacia->idpadre = 0;
				$data['categoria'] = $categoriavacia;
				$data['id'] = 0;
				$data['categorias'] = $this->Categories_model->all();
				if($id > 0){//verificar si viene el id de categoria
					$categoria = $this->Categories_model->getById($id);
					$data['id'] = $id;
					if($categoria){
						$data['categoria'] = $categoria;
					}else{
						redirect(site_url('categories'));
					}
				}
				$this->load->view('categories/categories_form',$data);
			}
		}
	}

	public function store()
	{
		$this->load->model('Categories_model');
		$id = $this->input->post('id');
		$idpadre = $this->input->post('cat_padre');
		$nombre = $this->input->post('nombre');
		if($id == 0){//create
			$result = $this->Categories_model->create($nombre,$idpadre);
			if($result) {
				$this->session->set_flashdata('tipomsg', 'success');
				$this->session->set_flashdata('msg', 'Categoria creada');
				echo 'Ok';
				return;
			} else {
				// send errors
				$this->session->set_flashdata('tipomsg', 'danger');
				$this->session->set_flashdata('msg', 'Error creando categoria');
				echo "Error creando categoria";
				return;
			}
		}else{//update
			$result = $this->Categories_model->update($id,$nombre,$idpadre);
			if($result) {
				$this->session->set_flashdata('tipomsg', 'success');
				$this->session->set_flashdata('msg', 'Categoria actualizada');
				echo "Ok";
				return;
			} else {
				// send errors
				$this->session->set_flashdata('tipomsg', 'danger');
				$this->session->set_flashdata('msg', 'Error actualizando categoria');
				echo "Error actualizando categoria";
				return;
			}
		}
	}

	public function delete()
	{
		$this->load->model('Categories_model');
		$id = $this->input->post('id');
		if($id > 0){
			$result = $this->Categories_model->delete($id);
			if($result == "Ok") {
				$this->session->set_flashdata('tipomsg', 'success');
				$this->session->set_flashdata('msg', 'Categoria eliminada');
				echo 'Ok';
				return;
			} else {
				// send errors
				$errorespecifico = "";
				if($result == "error"){
					$errorespecifico = ", La categoria tiene productos asignados";
				}
				$this->session->set_flashdata('tipomsg', 'danger');
				$this->session->set_flashdata('msg', 'Error eliminando categoria'.$errorespecifico);
				echo "Error eliminando categoria".$errorespecifico;
				return;
			}
		}else{
			redirect(site_url('categories'));
		}
	}
}
